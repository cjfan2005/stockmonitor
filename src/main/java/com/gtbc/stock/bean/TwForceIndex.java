package com.gtbc.stock.bean;
// Generated Aug 14, 2016 11:38:56 AM by Hibernate Tools 4.3.4.Final

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * TwForceIndex generated by hbm2java
 */
@Entity
@Table(name = "tw_force_index", catalog = "GTBC")
public class TwForceIndex implements java.io.Serializable {

	private TwForceIndexId id;
	private Float priceDiff;
	private Float buy1;
	private Float sale1;
	private Float buy2;
	private Float sale2;
	private Float sumBuy;
	private Float sumSale;
	private Integer buyCnt;
	private Integer saleCnt;
	private Integer day20buyCnt;
	private Integer day20saleCnt;
	private Double forceIndex20;
	private Integer day14buyCnt;
	private Integer day14saleCnt;
	private Double forceIndex14;
	private Integer day6buyCnt;
	private Integer day6saleCnt;
	private Double forceIndex6;

	public TwForceIndex() {
	}

	public TwForceIndex(TwForceIndexId id) {
		this.id = id;
	}

	public TwForceIndex(TwForceIndexId id, Float priceDiff, Float buy1, Float sale1, Float buy2, Float sale2,
			Float sumBuy, Float sumSale, Integer buyCnt, Integer saleCnt, Integer day20buyCnt, Integer day20saleCnt,
			Double forceIndex20, Integer day14buyCnt, Integer day14saleCnt, Double forceIndex14, Integer day6buyCnt,
			Integer day6saleCnt, Double forceIndex6) {
		this.id = id;
		this.priceDiff = priceDiff;
		this.buy1 = buy1;
		this.sale1 = sale1;
		this.buy2 = buy2;
		this.sale2 = sale2;
		this.sumBuy = sumBuy;
		this.sumSale = sumSale;
		this.buyCnt = buyCnt;
		this.saleCnt = saleCnt;
		this.day20buyCnt = day20buyCnt;
		this.day20saleCnt = day20saleCnt;
		this.forceIndex20 = forceIndex20;
		this.day14buyCnt = day14buyCnt;
		this.day14saleCnt = day14saleCnt;
		this.forceIndex14 = forceIndex14;
		this.day6buyCnt = day6buyCnt;
		this.day6saleCnt = day6saleCnt;
		this.forceIndex6 = forceIndex6;
	}

	@EmbeddedId

	@AttributeOverrides({
			@AttributeOverride(name = "tradingDate", column = @Column(name = "TradingDate", nullable = false, length = 10)),
			@AttributeOverride(name = "stockId", column = @Column(name = "StockId", nullable = false, length = 8)) })
	public TwForceIndexId getId() {
		return this.id;
	}

	public void setId(TwForceIndexId id) {
		this.id = id;
	}

	@Column(name = "PriceDiff", precision = 12, scale = 0)
	public Float getPriceDiff() {
		return this.priceDiff;
	}

	public void setPriceDiff(Float priceDiff) {
		this.priceDiff = priceDiff;
	}

	@Column(name = "Buy1", precision = 12, scale = 0)
	public Float getBuy1() {
		return this.buy1;
	}

	public void setBuy1(Float buy1) {
		this.buy1 = buy1;
	}

	@Column(name = "Sale1", precision = 12, scale = 0)
	public Float getSale1() {
		return this.sale1;
	}

	public void setSale1(Float sale1) {
		this.sale1 = sale1;
	}

	@Column(name = "Buy2", precision = 12, scale = 0)
	public Float getBuy2() {
		return this.buy2;
	}

	public void setBuy2(Float buy2) {
		this.buy2 = buy2;
	}

	@Column(name = "Sale2", precision = 12, scale = 0)
	public Float getSale2() {
		return this.sale2;
	}

	public void setSale2(Float sale2) {
		this.sale2 = sale2;
	}

	@Column(name = "SumBuy", precision = 12, scale = 0)
	public Float getSumBuy() {
		return this.sumBuy;
	}

	public void setSumBuy(Float sumBuy) {
		this.sumBuy = sumBuy;
	}

	@Column(name = "SumSale", precision = 12, scale = 0)
	public Float getSumSale() {
		return this.sumSale;
	}

	public void setSumSale(Float sumSale) {
		this.sumSale = sumSale;
	}

	@Column(name = "BuyCnt")
	public Integer getBuyCnt() {
		return this.buyCnt;
	}

	public void setBuyCnt(Integer buyCnt) {
		this.buyCnt = buyCnt;
	}

	@Column(name = "SaleCnt")
	public Integer getSaleCnt() {
		return this.saleCnt;
	}

	public void setSaleCnt(Integer saleCnt) {
		this.saleCnt = saleCnt;
	}

	@Column(name = "Day20BuyCnt")
	public Integer getDay20buyCnt() {
		return this.day20buyCnt;
	}

	public void setDay20buyCnt(Integer day20buyCnt) {
		this.day20buyCnt = day20buyCnt;
	}

	@Column(name = "Day20SaleCnt")
	public Integer getDay20saleCnt() {
		return this.day20saleCnt;
	}

	public void setDay20saleCnt(Integer day20saleCnt) {
		this.day20saleCnt = day20saleCnt;
	}

	@Column(name = "ForceIndex20", precision = 22, scale = 0)
	public Double getForceIndex20() {
		return this.forceIndex20;
	}

	public void setForceIndex20(Double forceIndex20) {
		this.forceIndex20 = forceIndex20;
	}

	@Column(name = "Day14BuyCnt")
	public Integer getDay14buyCnt() {
		return this.day14buyCnt;
	}

	public void setDay14buyCnt(Integer day14buyCnt) {
		this.day14buyCnt = day14buyCnt;
	}

	@Column(name = "Day14SaleCnt")
	public Integer getDay14saleCnt() {
		return this.day14saleCnt;
	}

	public void setDay14saleCnt(Integer day14saleCnt) {
		this.day14saleCnt = day14saleCnt;
	}

	@Column(name = "ForceIndex14", precision = 22, scale = 0)
	public Double getForceIndex14() {
		return this.forceIndex14;
	}

	public void setForceIndex14(Double forceIndex14) {
		this.forceIndex14 = forceIndex14;
	}

	@Column(name = "Day6BuyCnt")
	public Integer getDay6buyCnt() {
		return this.day6buyCnt;
	}

	public void setDay6buyCnt(Integer day6buyCnt) {
		this.day6buyCnt = day6buyCnt;
	}

	@Column(name = "Day6SaleCnt")
	public Integer getDay6saleCnt() {
		return this.day6saleCnt;
	}

	public void setDay6saleCnt(Integer day6saleCnt) {
		this.day6saleCnt = day6saleCnt;
	}

	@Column(name = "ForceIndex6", precision = 22, scale = 0)
	public Double getForceIndex6() {
		return this.forceIndex6;
	}

	public void setForceIndex6(Double forceIndex6) {
		this.forceIndex6 = forceIndex6;
	}

}
