package com.gtbc.stock.bean;
// Generated Aug 14, 2016 2:50:47 PM by Hibernate Tools 4.3.4.Final

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * TwStockOscillator generated by hbm2java
 */
@Entity
@Table(name = "tw_stock_oscillator", catalog = "GTBC")
public class TwStockOscillator implements java.io.Serializable {

	private TwStockOscillatorId id;
	private Double close;
	private Double slowEma;
	private Double fastEma;
	private Double macd;
	private Double signal;
	private Double histogramVal;
	private Integer conHistoVal;

	public TwStockOscillator() {
	}

	public TwStockOscillator(TwStockOscillatorId id) {
		this.id = id;
	}

	public TwStockOscillator(TwStockOscillatorId id, Double close, Double slowEma, Double fastEma, Double macd,
			Double signal, Double histogramVal, Integer conHistoVal) {
		this.id = id;
		this.close = close;
		this.slowEma = slowEma;
		this.fastEma = fastEma;
		this.macd = macd;
		this.signal = signal;
		this.histogramVal = histogramVal;
		this.conHistoVal = conHistoVal;
	}

	@EmbeddedId

	@AttributeOverrides({
			@AttributeOverride(name = "tradingDate", column = @Column(name = "TradingDate", nullable = false, length = 10)),
			@AttributeOverride(name = "stockId", column = @Column(name = "StockId", nullable = false, length = 8)) })
	public TwStockOscillatorId getId() {
		return this.id;
	}

	public void setId(TwStockOscillatorId id) {
		this.id = id;
	}

	@Column(name = "Close", precision = 11, scale = 0)
	public Double getClose() {
		return this.close;
	}

	public void setClose(Double close) {
		this.close = close;
	}

	@Column(name = "SlowEMA", precision = 22, scale = 0)
	public Double getSlowEma() {
		return this.slowEma;
	}

	public void setSlowEma(Double slowEma) {
		this.slowEma = slowEma;
	}

	@Column(name = "FastEMA", precision = 22, scale = 0)
	public Double getFastEma() {
		return this.fastEma;
	}

	public void setFastEma(Double fastEma) {
		this.fastEma = fastEma;
	}

	@Column(name = "MACD", precision = 22, scale = 0)
	public Double getMacd() {
		return this.macd;
	}

	public void setMacd(Double macd) {
		this.macd = macd;
	}

	@Column(name = "Signal", precision = 22, scale = 0)
	public Double getSignal() {
		return this.signal;
	}

	public void setSignal(Double signal) {
		this.signal = signal;
	}

	@Column(name = "HistogramVal", precision = 22, scale = 0)
	public Double getHistogramVal() {
		return this.histogramVal;
	}

	public void setHistogramVal(Double histogramVal) {
		this.histogramVal = histogramVal;
	}

	@Column(name = "ConHistoVal")
	public Integer getConHistoVal() {
		return this.conHistoVal;
	}

	public void setConHistoVal(Integer conHistoVal) {
		this.conHistoVal = conHistoVal;
	}

}
