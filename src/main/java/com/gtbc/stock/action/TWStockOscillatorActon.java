package com.gtbc.stock.action;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.gtbc.interceptor.BaseAction;
import com.gtbc.stock.dao.TwStockOscillatorDAO;
import com.gtbc.util.PageParameter;

public class TWStockOscillatorActon  extends BaseAction {
	private static final Logger logger = Logger.getLogger(TWStockOscillatorActon.class);

	private String stockId;
	private int conHistoVal;
	private double close, slowEMA, fastEMA, MACD, signal, histogramVal;
	
	//user input parameters for the Oscillator Algorithm
	private int slowEMAParam, fastEMAParam, signalParam;
	
	// jqgrid
	private List<Map<String, Object>> rows;
	private Map result, resultStep;
	private int total = 0; // 總頁數
	private int records = 0; // 資料總筆數
	private int pageSize = 0, pageSizeSF = 0;
	private int currentPageNo = 0;
	private String sort; 
	private String orderBy;
	
	@Autowired
	private TwStockOscillatorDAO twSO;
	
	public String oscillatorInit() {
		return "success";
	}
	
	
	@SuppressWarnings("unchecked")
	public String oscillatorReult() {
		PageParameter page = new PageParameter();
		page.setCurrentPageNo(currentPageNo);
		page.setOrderBy(orderBy);
		page.setSort(sort);
		page.setPageSize(pageSize);
		
		if(stockId.isEmpty())
			stockId = "2330";
		
		if(slowEMAParam==0 || fastEMAParam==0 || signalParam==0){
			slowEMAParam=26; fastEMAParam=12; signalParam=9;
		}
		
		List<Map<String, Object>> objList = twSO.oscillatorCallCenter(stockId, slowEMAParam, fastEMAParam, signalParam);
		
		int count = objList.size();
		
		objList = dataPagination(objList, page);

		rows = objList;

		int total = count % pageSize == 0 ? (count / pageSize) : (count / pageSize + 1);

		result = new HashMap<String, Object>();
		result.put("pageSize", pageSize);
		result.put("total", total);
		result.put("count", count);
		result.put("currentPageNo", currentPageNo);
		result.put("rows", rows);
		
		return "success";
	}

	//getter & setter
	public String getStockId() {
		return stockId;
	}

	public void setStockId(String stockId) {
		this.stockId = stockId;
	}

	public int getConHistoVal() {
		return conHistoVal;
	}

	public void setConHistoVal(int conHistoVal) {
		this.conHistoVal = conHistoVal;
	}

	public double getClose() {
		return close;
	}

	public void setClose(double close) {
		this.close = close;
	}

	public double getSlowEMA() {
		return slowEMA;
	}

	public void setSlowEMA(double slowEMA) {
		this.slowEMA = slowEMA;
	}

	public double getFastEMA() {
		return fastEMA;
	}

	public void setFastEMA(double fastEMA) {
		this.fastEMA = fastEMA;
	}

	public double getMACD() {
		return MACD;
	}

	public void setMACD(double mACD) {
		MACD = mACD;
	}

	public double getSignal() {
		return signal;
	}

	public void setSignal(double signal) {
		this.signal = signal;
	}

	public double getHistogramVal() {
		return histogramVal;
	}

	public void setHistogramVal(double histogramVal) {
		this.histogramVal = histogramVal;
	}

	public List<Map<String, Object>> getRows() {
		return rows;
	}

	public void setRows(List<Map<String, Object>> rows) {
		this.rows = rows;
	}

	public Map getResult() {
		return result;
	}

	public void setResult(Map result) {
		this.result = result;
	}

	public Map getResultStep() {
		return resultStep;
	}

	public void setResultStep(Map resultStep) {
		this.resultStep = resultStep;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public int getRecords() {
		return records;
	}

	public void setRecords(int records) {
		this.records = records;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getPageSizeSF() {
		return pageSizeSF;
	}

	public void setPageSizeSF(int pageSizeSF) {
		this.pageSizeSF = pageSizeSF;
	}

	public int getCurrentPageNo() {
		return currentPageNo;
	}

	public void setCurrentPageNo(int currentPageNo) {
		this.currentPageNo = currentPageNo;
	}

	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

	public String getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}

	public int getSlowEMAParam() {
		return slowEMAParam;
	}

	public void setSlowEMAParam(int slowEMAParam) {
		this.slowEMAParam = slowEMAParam;
	}

	public int getFastEMAParam() {
		return fastEMAParam;
	}

	public void setFastEMAParam(int fastEMAParam) {
		this.fastEMAParam = fastEMAParam;
	}

	public int getSignalParam() {
		return signalParam;
	}

	public void setSignalParam(int signalParam) {
		this.signalParam = signalParam;
	}
	
}
