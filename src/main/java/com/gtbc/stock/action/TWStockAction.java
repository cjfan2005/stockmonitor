package com.gtbc.stock.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.gtbc.interceptor.BaseAction;
import com.gtbc.stock.dao.TwForceIndexDAO;
import com.gtbc.stock.dao.TwHistoricalPricesDAO;
import com.gtbc.util.PageParameter;

public class TWStockAction extends BaseAction {
	private static final Logger logger = Logger.getLogger(TWStockAction.class);
	private String stockId, name, tradingDateStr, tradingDateEnd;
	private Date tradingDate;
	
	// jqgrid
	private List<Map<String, Object>> rows;
	private Map result, resultStep;
	private int total = 0; // 總頁數
	private int records = 0; // 資料總筆數
	private int pageSize = 0, pageSizeSF = 0;
	private int currentPageNo = 0;
	private String sort;
	private String orderBy;
	

	@Autowired
	private TwHistoricalPricesDAO twHPDAO;
	@Autowired
	private TwForceIndexDAO twFIDAO;
	
    public String ListStock() {
    	
    	return "success";
    }
    
    @SuppressWarnings("unchecked")
	public String ListStockCondition() {
    	PageParameter page = new PageParameter();
		page.setCurrentPageNo(currentPageNo);
		page.setOrderBy(orderBy);
		page.setSort(sort);
		page.setPageSize(pageSize);
		
		//default show TSMC
		if(stockId == null || stockId.isEmpty()) {
			stockId ="2330";
		}
		
		
		if((tradingDateStr != null && !tradingDateStr.toString().isEmpty())
			&& (tradingDateEnd != null && !tradingDateEnd.toString().isEmpty())) {
			//
		}
		
		List<Map<String, Object>> objList = twHPDAO.getStockCondition(stockId, name, tradingDateStr, tradingDateEnd, sort, orderBy);
		int count = objList.size();

		objList = dataPagination(objList, page);

		Iterator<?> its = objList.iterator();

		List<Map<String, Object>> stockList = new ArrayList<Map<String, Object>>();
		Map<String, Object> stockItem;
		while (its.hasNext()) {
			stockItem = new HashMap<String, Object>();

			Object[] obj = (Object[]) its.next();
			
			stockItem.put("TradingDate", obj[0]);
			stockItem.put("StockId", obj[1]);
			stockItem.put("Name", obj[2]);
			stockItem.put("Price", obj[3]);
			stockItem.put("Volume", obj[4]);
			stockItem.put("Open", obj[5]);
			stockItem.put("High", obj[6]);
			stockItem.put("Low", obj[7]);

			stockList.add(stockItem);
		}

		rows = stockList;

		int total = count % pageSize == 0 ? (count / pageSize) : (count / pageSize + 1);

		result = new HashMap<String, Object>();
		result.put("pageSize", pageSize);
		result.put("total", total);
		result.put("count", count);
		result.put("currentPageNo", currentPageNo);
		result.put("rows", rows);
		
		return SUCCESS;
    }
    
    public String ForceIndex() {
    	return SUCCESS;
    }
    
    //show best buy on screen.
    @SuppressWarnings("unchecked")
	public String bestBuyStock() {
    	PageParameter page = new PageParameter();
		page.setCurrentPageNo(currentPageNo);
		page.setOrderBy(orderBy);
		page.setSort(sort);
		page.setPageSize(pageSize);
		
		//TODO
		List<Map<String, Object>> objList = twFIDAO.getBestBuyStock();
		int count = objList.size();

		objList = dataPagination(objList, page);

		Iterator<?> its = objList.iterator();

		List<Map<String, Object>> bestBuyList = new ArrayList<Map<String, Object>>();
		Map<String, Object> bbItem;
		while (its.hasNext()) {
			bbItem = new HashMap<String, Object>();

			Object[] obj = (Object[]) its.next();
			
			bbItem.put("Name&Id", obj[1]+"["+obj[2]+"]");
			bbItem.put("AccountingDate", obj[0]);
			bbItem.put("Name", obj[1]);
			bbItem.put("StockId", obj[2]);
			bbItem.put("ForceIndexVal", obj[3]);
			

			bestBuyList.add(bbItem);
		}

		rows = bestBuyList;

		int total = count % pageSize == 0 ? (count / pageSize) : (count / pageSize + 1);

		result = new HashMap<String, Object>();
		result.put("pageSize", pageSize);
		result.put("total", total);
		result.put("count", count);
		result.put("currentPageNo", currentPageNo);
		result.put("rows", rows);
		
    	return SUCCESS;
    }
    
    @SuppressWarnings("unchecked")
    public String FIListCond() {
    	PageParameter page = new PageParameter();
		page.setCurrentPageNo(currentPageNo);
		page.setOrderBy(orderBy);
		page.setSort(sort);
		page.setPageSize(pageSize);
		
		//TODO
		List<Map<String, Object>> objList = twFIDAO.getFIData(stockId, name, sort, orderBy);
		int count = objList.size();

		objList = dataPagination(objList, page);

		Iterator<?> its = objList.iterator();

		List<Map<String, Object>> forceIndexList = new ArrayList<Map<String, Object>>();
		Map<String, Object> fiItem;
		while (its.hasNext()) {
			fiItem = new HashMap<String, Object>();

			Object[] obj = (Object[]) its.next();
			
			fiItem.put("Name&Id", obj[2]+" ["+obj[1]+"]");
			fiItem.put("TradingDate", obj[0]);
			fiItem.put("StockId", obj[1]);
			fiItem.put("Name", obj[2]);
			fiItem.put("Volume", obj[3]);
			fiItem.put("Open", obj[4]);
			fiItem.put("High", obj[5]);
			fiItem.put("Low", obj[6]);
			fiItem.put("Price", obj[7]);
			fiItem.put("PriceDiff", obj[8]);
			fiItem.put("Buy1", obj[9]);
			fiItem.put("Buy2", obj[10]);
			fiItem.put("Sale1", obj[11]);
			fiItem.put("Sale2", obj[12]);
			fiItem.put("SumBuy", obj[13]);
			fiItem.put("SumSale", obj[14]);
			fiItem.put("BuyCnt", obj[15]);
			fiItem.put("SaleCnt", obj[16]);
			fiItem.put("Day20BuyCnt", obj[17]);
			fiItem.put("Day20SaleCnt", obj[18]);
			fiItem.put("Day14BuyCnt", obj[19]);
			fiItem.put("Day14SaleCnt", obj[20]);
			fiItem.put("Day6BuyCnt", obj[21]);
			fiItem.put("Day6SaleCnt", obj[22]);
			fiItem.put("ForceIndex20", obj[23]);
			fiItem.put("ForceIndex14", obj[24]);
			fiItem.put("ForceIndex6", obj[25]);

			forceIndexList.add(fiItem);
		}

		rows = forceIndexList;

		int total = count % pageSize == 0 ? (count / pageSize) : (count / pageSize + 1);

		result = new HashMap<String, Object>();
		result.put("pageSize", pageSize);
		result.put("total", total);
		result.put("count", count);
		result.put("currentPageNo", currentPageNo);
		result.put("rows", rows);
		
    	return SUCCESS;
    }
    
    @SuppressWarnings({ "unchecked", "unchecked" })
	public String getBestBuyShortData() {
		if ((stockId == null || stockId.isEmpty())) {
			return null;
		}
		
		List<Map<String, Object>> objStepList = twFIDAO.getBestBuyShortData(stockId);
		
		Iterator<?> itsStep = objStepList.iterator();

		List<Map<String, Object>> stepList = new ArrayList<Map<String, Object>>();
		Map<String, Object> stepMap;

		//arrange data
		while (itsStep.hasNext()) {
			stepMap = new HashMap<String, Object>();

			Object[] obj = (Object[]) itsStep.next();

			stepMap.put("TradingDate", obj[0]);
			stepMap.put("Open", obj[1]);
			stepMap.put("High", obj[2]);
			stepMap.put("Low", obj[3]);
			stepMap.put("Price", obj[4]);
			stepMap.put("Volume", obj[5]);
			stepMap.put("ForceIndex6", obj[6]);
			stepMap.put("ForceIndex14", obj[7]);
			stepMap.put("ForceIndex20", obj[8]);
			stepList.add(stepMap);
		}

		rows = stepList;
		resultStep = new HashMap<String, Object>();
		resultStep.put("rows", rows);
		return SUCCESS;
	}
    
    public String initGenChart() {
    	return "success";
    }
 
	@SuppressWarnings("unchecked")
	public String genChart() {
		Map<Integer, Float> d5MaMap = new HashMap<Integer, Float>();
		Map<Integer, Float> d10MaMap = new HashMap<Integer, Float>();
		Map<Integer, Float> d20MaMap = new HashMap<Integer, Float>();
		Map<Integer, Float> d30MaMap = new HashMap<Integer, Float>();
		
		
    	if(stockId.length()>0 && stockId != null) {
    		
    		String[] parSid = stockId.trim().split(",");
    		stockId = parSid[0];
    	}
    	
    	List<Map<String, Object>> objList = twHPDAO.getDataForChart(stockId);
		
    	Iterator<?> its = objList.iterator();

		List<Map<String, Object>> stockList = new ArrayList<Map<String, Object>>();
		
		Map<String, Object> stockItem;
		result = new HashMap<String, Object>();
		String symbol = null, name = "gtbc Inc.";
		int i = 0;
		while (its.hasNext()) {
			stockItem = new HashMap<String, Object>();
            
			Object[] obj = (Object[]) its.next();
			symbol = obj[2].toString() + " " + obj[1].toString();
			
			stockItem.put("time", obj[0].toString());
			stockItem.put("close", obj[3]);//Price收盤價
			stockItem.put("volume", obj[4]);
			stockItem.put("open", obj[5]);
			stockItem.put("high", obj[6]);
			stockItem.put("low", obj[7]);
			
			float ma5=0 , ma10=0, ma20=0, ma30=0;
			//5MA
			d5MaMap.put(i, Float.parseFloat(obj[3].toString()));
			if(i>5) {
				int x=0;
				for(x=i; x>i-5; x--) {
					ma5 += d5MaMap.get(x);
				}
				ma5 = (float)ma5/(float)5;
				stockItem.put("ma5", ma5);
			}else {
				stockItem.put("ma5", 0);
			}
			
			//10MA
			d10MaMap.put(i, Float.parseFloat(obj[3].toString()));
			if(i>10) {
				int x=0;
				for(x=i; x>i-10; x--) {
					ma10 += d10MaMap.get(x);
				}
				ma10 = (float)ma10/(float)10;
				stockItem.put("ma10", ma10);
			}else 
				stockItem.put("ma10", 0);
			
			//20MA
			d20MaMap.put(i, Float.parseFloat(obj[3].toString()));
			if(i>20) {
				int x=0;
				for(x=i; x>i-20; x--) {
					ma20 += d20MaMap.get(x);
				}
				ma20 = (float)ma20/(float)20;
				stockItem.put("ma20", ma20);
			}else 
				stockItem.put("ma20", 0);
			
		
			//30MA
			d30MaMap.put(i, Float.parseFloat(obj[3].toString()));
			if(i>30) {
				int x=0;
				for(x=i; x>i-30; x--) {
					ma30 += d30MaMap.get(x);
				}
				ma30 = (float)ma30/(float)30;
				stockItem.put("ma30", ma30);
			}else 
				stockItem.put("ma30", 0);

//			stockItem.put("chg", 64);
//			stockItem.put("percent", 64);
//			stockItem.put("turrate", 64);
//			stockItem.put("dif", 16);
//			stockItem.put("dea", 16);
//			stockItem.put("macd", 16);
			
			stockList.add(stockItem);
			i++;
		}
		
		result.put("symbol", symbol);
		result.put("name", name);
		result.put("list", stockList);
    	return "success";
    }
	
	
	public String initGenChartFI() {
    	return "success";
    }
 
	@SuppressWarnings("unchecked")
	public String genChartFI() {
		Map<Integer, Float> d5MaMap = new HashMap<Integer, Float>();
		Map<Integer, Float> d10MaMap = new HashMap<Integer, Float>();
		Map<Integer, Float> d20MaMap = new HashMap<Integer, Float>();
		Map<Integer, Float> d30MaMap = new HashMap<Integer, Float>();
		
		
    	if(stockId.length()>0 && stockId != null) {
    		String[] parSid = stockId.trim().split(",");
    		stockId = parSid[0];
    	}
    	
    	List<Map<String, Object>> objList = twFIDAO.getDataForChart(stockId);
		
    	Iterator<?> its = objList.iterator();

		List<Map<String, Object>> stockList = new ArrayList<Map<String, Object>>();
		
		Map<String, Object> stockItem;
		result = new HashMap<String, Object>();
		String symbol = null, name = "gtbc Inc.";
		int i = 0;
		while (its.hasNext()) {
			stockItem = new HashMap<String, Object>();
            
			Object[] obj = (Object[]) its.next();
			symbol = obj[2].toString() + " " + obj[1].toString();
			
			stockItem.put("time", obj[0].toString());
			stockItem.put("close", obj[3]);//Price收盤價
			stockItem.put("volume", obj[4]);
			stockItem.put("open", obj[5]);
			stockItem.put("high", obj[6]);
			stockItem.put("low", obj[7]);
			
			float ma5=0 , ma10=0, ma20=0, ma30=0;
			//5MA
			d5MaMap.put(i, Float.parseFloat(obj[3].toString()));
			if(i>5) {
				int x=0;
				for(x=i; x>i-5; x--) {
					ma5 += d5MaMap.get(x);
				}
				ma5 = (float)ma5/(float)5;
				stockItem.put("ma5", ma5);
			}else {
				stockItem.put("ma5", 0);
			}
			
			//10MA
			d10MaMap.put(i, Float.parseFloat(obj[3].toString()));
			if(i>10) {
				int x=0;
				for(x=i; x>i-10; x--) {
					ma10 += d10MaMap.get(x);
				}
				ma10 = (float)ma10/(float)10;
				stockItem.put("ma10", ma10);
			}else 
				stockItem.put("ma10", 0);
			
			//20MA
			d20MaMap.put(i, Float.parseFloat(obj[3].toString()));
			if(i>20) {
				int x=0;
				for(x=i; x>i-20; x--) {
					ma20 += d20MaMap.get(x);
				}
				ma20 = (float)ma20/(float)20;
				stockItem.put("ma20", ma20);
			}else 
				stockItem.put("ma20", 0);
			
		
			//30MA
			d30MaMap.put(i, Float.parseFloat(obj[3].toString()));
			if(i>30) {
				int x=0;
				for(x=i; x>i-30; x--) {
					ma30 += d30MaMap.get(x);
				}
				ma30 = (float)ma30/(float)30;
				stockItem.put("ma30", ma30);
			}else 
				stockItem.put("ma30", 0);

			if((obj[8]==null || obj[9]==null || obj[10]==null ) || 
			   (obj[8].toString().length()==0 || obj[9].toString().length()==0 || obj[10].toString().length()==0)) {
				stockItem.put("fi6", 0);
				stockItem.put("fi14", 0);
				stockItem.put("fi20", 0);
			}else {
				stockItem.put("fi6", obj[8]);
				stockItem.put("fi14", obj[9]);
				stockItem.put("fi20", obj[10]);
			}
			
//			stockItem.put("dif", 16);
//			stockItem.put("dea", 16);
//			stockItem.put("macd", 16);
			
			stockList.add(stockItem);
			i++;
		}
		
		result.put("symbol", symbol);
		result.put("name", name);
		result.put("list", stockList);
    	return "success";
    }
    
	public String executeForceIndexInit() {
		return "success";
	}
	
	//計算force Index.
	@SuppressWarnings("unchecked")
	public String executeForceIndex() {
		PageParameter page = new PageParameter();
		page.setCurrentPageNo(currentPageNo);
		page.setOrderBy(orderBy);
		page.setSort(sort);
		page.setPageSize(pageSize);
		
		//if query condition are null, return empty list.
		if ((stockId == null || stockId.isEmpty()) && (name == null || name.isEmpty())) {
			return null;
		}
		
		List<Map<String, Object>> objList = twFIDAO.FICallCenter(stockId, name);
		
		int count = objList.size();
		
		objList = dataPagination(objList, page);

		rows = objList;

		int total = count % pageSize == 0 ? (count / pageSize) : (count / pageSize + 1);

		result = new HashMap<String, Object>();
		result.put("pageSize", pageSize);
		result.put("total", total);
		result.put("count", count);
		result.put("currentPageNo", currentPageNo);
		result.put("rows", rows);
		
		return "success";
	}
	
	
	//手動點選啟動計算best buy stocks.
	public String executeBestBuyManual() {
		String algorithmResult = "";
		twFIDAO.BestBuyStockCallCenter();
		if(algorithmResult.equals("success"))
			logger.log(Level.INFO, "Manually execute force index Alogrithm finished");
		else 
			logger.log(Level.DEBUG, "Manually execute force index Alogrithm failed");
			
		return "success";
	}
	
	public String getStockId() {
		return stockId;
	}

	public void setStockId(String stockId) {
		this.stockId = stockId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTradingDateStr() {
		return tradingDateStr;
	}

	public void setTradingDateStr(String tradingDateStr) {
		this.tradingDateStr = tradingDateStr;
	}

	public String getTradingDateEnd() {
		return tradingDateEnd;
	}

	public void setTradingDateEnd(String tradingDateEnd) {
		this.tradingDateEnd = tradingDateEnd;
	}

	public Date getTradingDate() {
		return tradingDate;
	}

	public void setTradingDate(Date tradingDate) {
		this.tradingDate = tradingDate;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public int getRecords() {
		return records;
	}

	public void setRecords(int records) {
		this.records = records;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getPageSizeSF() {
		return pageSizeSF;
	}

	public void setPageSizeSF(int pageSizeSF) {
		this.pageSizeSF = pageSizeSF;
	}

	public int getCurrentPageNo() {
		return currentPageNo;
	}

	public void setCurrentPageNo(int currentPageNo) {
		this.currentPageNo = currentPageNo;
	}

	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

	public String getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}

	public List<Map<String, Object>> getRows() {
		return rows;
	}

	public void setRows(List<Map<String, Object>> rows) {
		this.rows = rows;
	}

	public Map getResult() {
		return result;
	}

	public void setResult(Map result) {
		this.result = result;
	}

	public Map getResultStep() {
		return resultStep;
	}

	public void setResultStep(Map resultStep) {
		this.resultStep = resultStep;
	}


}
