package com.gtbc.stock.action;

import java.io.File;
import java.io.FileInputStream;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Row;
import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;

import com.gtbc.interceptor.BaseAction;
import com.gtbc.stock.bean.TwHistoricalPrices;
import com.gtbc.stock.bean.TwHistoricalPricesId;
import com.gtbc.stock.dao.TwHistoricalPricesDAO;

public class ParseExcelAction extends BaseAction {
	@Autowired
	private TwHistoricalPricesDAO twHPDAO;

//	public static String excelParsing() {
	public String parseExcel() {
		try
        {
			//for(int k=0; k<=200 ; k++) {
	            FileInputStream file = new FileInputStream(new File("/home/kent/dev/DailyDDE/GetStocksAutomatic.xlsx"));
	 
	            //Create Workbook instance holding reference to .xlsx file
	            XSSFWorkbook workbook = new XSSFWorkbook(file);
	 
	            //Get first/desired sheet from the workbook
	            XSSFSheet sheet = workbook.getSheetAt(0);
	 
	            
	            //Iterate through each rows one by one
	            Iterator<Row> rowIterator = sheet.iterator();
	            
	            int i=0;
	            StringBuffer sb = new StringBuffer();
	            while (rowIterator.hasNext()) 
	            {
	                Row row = rowIterator.next();
	                //For each row, iterate through all the columns
	                Iterator<Cell> cellIterator = row.cellIterator();
	                String rowRes = "";
	                while (cellIterator.hasNext()) 
	                {
	                    Cell cell = cellIterator.next();
	                    String cellRes = "";
	                    //Check the cell type and format accordingly
	                    switch (cell.getCellType()) 
	                    {
	                        case Cell.CELL_TYPE_NUMERIC:
	                        	if (HSSFDateUtil.isCellDateFormatted(cell)) {
	                                Date date = cell.getDateCellValue();
	                                cellRes = DateFormatUtils.format(date, "yyyy-MM-dd")+ "@";
	                            } else {
	                            	cellRes = String.valueOf(cell.getNumericCellValue())+ "@";
	                                //DecimalFormat df = new DecimalFormat("0");
	                                //cellRes = df.format(cellRes);
	                            }
	                            break;
	                            
	                        case Cell.CELL_TYPE_STRING:
	                        	cellRes = cell.getStringCellValue().toString() + "@";
	                            //System.out.print(cell.getStringCellValue() + "#");
	                            break;
	                    }
	                    rowRes += cellRes.trim();
	                }
	                i++;
	                if(rowRes.length()!=0)
	                	sb.append(rowRes+";");
	            }
	            file.close();
	            //System.out.println(sb);
	            String result = twHPDAO.excelToDatabase(arrageDataToBean(sb)); //get List Bean and inset into db. 
            //}
        } 
        catch (Exception e) {
            e.printStackTrace();
        }
		return "success";
    }
	
//	public static void arrageDataToBean(StringBuffer sb) throws ParseException {
	public List<TwHistoricalPrices> arrageDataToBean(StringBuffer sb) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String[] lines = sb.toString().trim().split(";");
		int i = 0;
		String stockId = "";
		
        List<TwHistoricalPrices> list = new ArrayList<TwHistoricalPrices>();
		for(String row: lines){
			if(i==1) {
				//System.out.println(row);
				String[] rowSplit = row.split("@");
				//System.out.println("rowSplit[0]: " + rowSplit[0].replace(".0", ""));
							
				stockId = rowSplit[0].replace(".0", "");
				
			} else if(i>2) {
				//System.out.println(row);
				TwHistoricalPrices twHP = new TwHistoricalPrices();
		        TwHistoricalPricesId twHPId = new TwHistoricalPricesId();
			    String[] rowResSplit = row.split("@");
			    String tradingDate = rowResSplit[0].toString();
			    float open = Float.parseFloat(rowResSplit[1]);
			    float high = Float.parseFloat(rowResSplit[2]);
			    float low = Float.parseFloat(rowResSplit[3]);
			    float price = Float.parseFloat(rowResSplit[4]);
			    String volume = rowResSplit[7].replace(".0", "");
			    float per = Float.parseFloat(rowResSplit[9]);
			    
			    twHP.setPrice(price);
			    twHP.setVolume(Integer.parseInt(volume));
			    twHP.setOpen(open);
			    twHP.setHigh(high);
			    twHP.setLow(low);
			    twHP.setPer(per);
			    twHPId.setTradingDate(sdf.parse(tradingDate));
			    twHPId.setStockId(stockId);
			    twHP.setId(twHPId);
			    list.add(twHP);
			}
			i++;
		}
		return list;
		//System.out.println("123");
	}
		
	public static void main(String[] args) 
    {
//		excelParsing();
		System.out.println("end of parsing excel...");
		
    }
}
