package com.gtbc.stock.action;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import java.io.File;
import java.io.FileInputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.ss.usermodel.Row;
import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;

import com.gtbc.interceptor.BaseAction;
import com.gtbc.stock.bean.TwHistoricalPrices;
import com.gtbc.stock.bean.TwHistoricalPricesId;
import com.gtbc.stock.dao.TwHistoricalPricesDAO;
import com.gtbc.util.Util;

public class ParseExcelDDEAction extends BaseAction {
	private static final Logger logger = Logger.getLogger(ParseExcelDDEAction.class);
	
	@Autowired
	private TwHistoricalPricesDAO twHPDAO;

//	public static String excelDDEParsing() {
	public String parseExcelDDE() {
		try
        {
			//String[] ddeFile = {"D:/DailyDDE/TWStock_OTM_DDE.xls", "D:/DailyDDE/TWStock_OTC_DDE.xls"};
			
			//String[] ddeFile = {"/Volumes/WD/dev/DailyDDE/TWStock_OTM_DDE.xls", "/Volumes/WD/dev/DailyDDE/TWStock_OTC_DDE.xls"};
			
			String[] ddeFile = {"/home/kent/dev/DailyDDE/TWStock_OTM_DDE.xls", "/home/kent/dev/DailyDDE/TWStock_OTC_DDE.xls"};
			
			for(int i=0; i < ddeFile.length; i++) {
				logger.log(Level.INFO, "parseExcelDDE, file="+ ddeFile[i]);
				
			
	            FileInputStream file = new FileInputStream(new File(ddeFile[i].toString()));
	            //FileInputStream file = new FileInputStream(new File("C:/Users/A622204/Downloads/TWStock_OTC_DDE.xlsx"));
	 
	            //Create Workbook instance holding reference to .xlsx file
	            //XSSFWorkbook workbook = new XSSFWorkbook(file);
	            HSSFWorkbook workbook = new HSSFWorkbook(file);////for .xls file
	 
	            //Get first/desired sheet from the workbook
	            //XSSFSheet sheet = workbook.getSheetAt(0);
	            HSSFSheet sheet = workbook.getSheetAt(0); //for .xls file
	 
	            
	            //Iterate through each rows one by one
	            Iterator<Row> rowIterator = sheet.iterator();
	            
	            
	            StringBuffer sb = new StringBuffer();
	            while (rowIterator.hasNext()) 
	            {
	                Row row = rowIterator.next();
	                //For each row, iterate through all the columns
	                Iterator<Cell> cellIterator = row.cellIterator();
	                String rowRes = "";
	                while (cellIterator.hasNext()) 
	                {
	                    Cell cell = cellIterator.next();
	                    String cellRes = "";
	                    //Check the cell type and format accordingly
	                    switch (cell.getCellType()) 
	                    {
	                        case Cell.CELL_TYPE_NUMERIC:
	                        	if (HSSFDateUtil.isCellDateFormatted(cell)) {
	                                Date date = cell.getDateCellValue();
	                                cellRes = DateFormatUtils.format(date, "yyyy-MM-dd")+ "@";
	                            } else {
	                            	cellRes = String.valueOf(cell.getNumericCellValue())+ "@";
	                            }
	                            break;
	                            
	                        case Cell.CELL_TYPE_STRING:
	                        	cellRes = cell.getStringCellValue().toString() + "@";
	                            //System.out.print(cell.getStringCellValue() + "#");
	                            break;
	                            
	                        case Cell.CELL_TYPE_FORMULA:
	                        	switch(cell.getCachedFormulaResultType()) {
		                            case Cell.CELL_TYPE_NUMERIC:
		                            	cellRes = String.valueOf(cell.getNumericCellValue())+ "@";
		                                //System.out.println("Last evaluated as: " + cell.getNumericCellValue());
		                                break;
		                            case Cell.CELL_TYPE_STRING:
		                            	cellRes = cell.getStringCellValue() + "@";
		                                //System.out.println("Last evaluated as \"" + cell.getRichStringCellValue() + "\"");
		                                break;
	                            }
	                			break;
	                    }
	                    rowRes += cellRes.trim();
	                }
	                if(rowRes.length()!=0)
	                	sb.append(rowRes+";");
	            }
	            file.close();
	            //System.out.println(sb);
	            //2016/07/05@1258@其祥-KY@42.0@1004.0@41.2@43.25@41.0@3.7@;
	            arrageDataToBean(sb);
	            String result = twHPDAO.excelDDEToDatabase(arrageDataToBean(sb)); //get List Bean and inset into db. 
			}
		} 
        catch (Exception e) {
            e.printStackTrace();
        }
		return "success";
    }
	
//	public static void arrageDataToBean(StringBuffer sb) throws ParseException {
	public List<TwHistoricalPrices> arrageDataToBean(StringBuffer sb) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String[] lines = sb.toString().trim().split(";");
		int i = 0;
		
		//2016/07/05@1258@其祥-KY@42.0@1004.0@41.2@43.25@41.0@3.7@;
		Date tradingDate, tradingDateGlobal  = null;
        List<TwHistoricalPrices> list = new ArrayList<TwHistoricalPrices>();
		for(String row: lines){
			if(i>0) {
				TwHistoricalPrices twHP = new TwHistoricalPrices();
		        TwHistoricalPricesId twHPId = new TwHistoricalPricesId();
			    String[] rowResSplit = row.split("@");
			    
			    if(rowResSplit[0].isEmpty() || rowResSplit[0].length()==0 || 
			       rowResSplit[0].equals("--") || rowResSplit[0].equals("1970/01/01") ) {
			    	tradingDate = tradingDateGlobal;
			    } else {
			    	String tdStr = rowResSplit[0]+ " 00:00:00";
			    	tradingDateGlobal = Util.getDateFromStr(tdStr);
			    }
			    
			    String stockId = rowResSplit[1];
			    String name, volume;
			    float price, open, high, low, priceChangeRatio, per;
			    
			    if(rowResSplit[2].isEmpty() || rowResSplit[2].length()==0 || rowResSplit[2].equals("--"))
			    	name = "not exist";
			    else
			    	name = rowResSplit[2];
			    
			    if(rowResSplit[3].isEmpty() || rowResSplit[3].length()==0 || rowResSplit[3].equals("--"))
			    	price = 0;
			    else
			    	price = Float.parseFloat(rowResSplit[3]);
			    
			    if(rowResSplit[4].isEmpty() || rowResSplit[4].length()==0 || rowResSplit[4].equals("--"))
			    	volume = "0";
			    else
			    	volume = rowResSplit[4].replace(".0", "");
			    
			    if(rowResSplit[5].isEmpty() || rowResSplit[5].length()==0 || rowResSplit[5].equals("--"))
			    	open = 0;
			    else
			    	open = Float.parseFloat(rowResSplit[5]);
			    
			    if(rowResSplit[6].isEmpty() || rowResSplit[6].length()==0 || rowResSplit[6].equals("--"))
			    	high = 0;
			    else
			    	high = Float.parseFloat(rowResSplit[6]);
			    
			    if(rowResSplit[7].isEmpty() || rowResSplit[7].length()==0 || rowResSplit[7].equals("--"))
			    	low = 0;
			    else
			    	low = Float.parseFloat(rowResSplit[7]);
			    
//			    if(rowResSplit[8].isEmpty() || rowResSplit[8].length()==0 || rowResSplit[8].equals("--"))
//			    	priceChangeRatio = 0;
//			    else
//			    	priceChangeRatio = Float.parseFloat(rowResSplit[8]);
			    
			    
			    per = 0;
			    
			    /**
			     * 如果開盤,收盤,量為0(open,price,volume==0),表示今天沒有交易,
			     * 不寫入垃圾資料。
			     */
			    if(open!=0 && price!=0) {
			    	//twHP.setName(name);
				    twHP.setPrice(price);
				    twHP.setVolume(Integer.parseInt(volume));
				    twHP.setOpen(open);
				    twHP.setHigh(high);
				    twHP.setLow(low);
				    //twHP.setPriceChangeRatio(priceChangeRatio);
				    //twHP.setPer(per);
				    twHPId.setTradingDate(tradingDateGlobal);
				    twHPId.setStockId(stockId);
				    twHP.setId(twHPId);
				    list.add(twHP);
			    } else {
			    	logger.log(Level.INFO, "Garbage Data, Stock: " + stockId +"["+name+"]");
			    }
			    //System.out.println("i=====>"+i + ", price==>" +price);
			}
			i++;
		}
		//System.out.println("123");
		return list;
	}
		
	public static void main(String[] args) 
    {
//		excelDDEParsing();
		System.out.println("end of parsing excel...");
		
    }
}
