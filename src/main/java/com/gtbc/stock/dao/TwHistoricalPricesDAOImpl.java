package com.gtbc.stock.dao;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.gtbc.stock.bean.TwHistoricalPrices;
import com.gtbc.stock.bean.TwHistoricalPricesId;
import com.gtbc.util.StringUtils;

public class TwHistoricalPricesDAOImpl implements TwHistoricalPricesDAO {
	private static final Logger logger = Logger.getLogger(TwHistoricalPricesDAOImpl.class);
	
	private SessionFactory sessionFactory;

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	public List<Map<String, Object>> getStockCondition(String stockId, String name, String tradingDateStr, String tradingDateEnd, String sort, String orderBy) {
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		Session session = sessionFactory.openSession();
		String sql = "";
		try {
			
			sql += " SELECT ";
			sql += " UNIX_TIMESTAMP(hp.TradingDate) as TradingDate, ";
//			sql += " DATE_FORMAT(hp.TradingDate,'%Y-%m-%d') as tradingDate, ";
			sql	+= "  hp.StockId, sm.Name, hp.Price, hp.Volume, hp.Open, hp.High, hp.Low ";
			sql += " FROM ";
			sql += " tw_historical_prices AS hp";
			sql += " LEFT JOIN tw_stock_map AS sm  ON hp.StockId = sm.StockId ";
			sql += " WHERE ";
			sql += " 1 = 1 ";
			
			if(StringUtils.isNotBlank(stockId))
				sql += " AND hp.StockId = '" + stockId + "' ";
			
			if(StringUtils.isNotBlank(name))
				sql += " AND sm.Name = '" + name + "' ";
			
			if(StringUtils.isNotBlank(tradingDateStr) && StringUtils.isNotBlank(tradingDateEnd)) {
				sql += " AND hp.TradingDate BETWEEN '" + tradingDateStr + "' AND  '" + tradingDateEnd + "' ";
			} else if (StringUtils.isNotBlank(stockId) || StringUtils.isNotBlank(name)) {
				//blank is fine.
			} else {
				//sql += " AND hp.TradingDate = CURRENT_DATE";
			}
				
//			if(StringUtils.isNotBlank(orderBy) && StringUtils.isNotBlank(sort)){
//				sql += " ORDER BY ";
//				sql += sort + " " + orderBy + " LIMIT 100";
//			} else {
				sql += " ORDER BY ";
				sql += " hp.TradingDate ASC;";
//			}
			
			Query query = session.createSQLQuery(sql.toString());
			list = query.list();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session.isOpen())
				session.close();
		}
		return list;
	}
	
	public List<Map<String, Object>> getDataForChart(String stockId) {
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		Session session = sessionFactory.openSession();
		String sql = "";
		try {
			
			//%a %b %d %H:%i:%S +0800 %Y = Mon May 25 00:00:00 +0800 2015
			sql += " SELECT ";
			sql += " DATE_FORMAT(TradingDate,'%a %b %d %H:%i:%S +0800 %Y'), ";
			sql	+= " StockId,SName,Price,Volume,OPEN,High,Low ";
			sql += " FROM ";
			sql += "   ( ";
			sql += "     SELECT";
			sql	+= "       hp1.TradingDate,hp1.StockId,sm.Name as SName,";
			sql += "       hp1.Price,hp1.Volume,hp1.OPEN,hp1.High,hp1.Low ";
			sql += "     FROM ";
			sql += "       tw_historical_prices AS hp1 ";
			sql += "       LEFT JOIN tw_stock_map AS sm ON hp1.StockId = sm.StockId ";
			sql += "     WHERE ";
			sql += "       hp1.StockId = '"+stockId+"'";
			sql += "     ORDER BY ";
			sql += "       hp1.TradingDate DESC";
			sql += "     LIMIT 180";
			sql += "   ) sub" ;
			sql += " ORDER BY TradingDate ASC;"; 
			
			Query query = session.createSQLQuery(sql.toString());
			list = query.list();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session.isOpen())
				session.close();
		}
		return list;
	}
	
	public String excelToDatabase(List<TwHistoricalPrices> twHPList) {
		
		Session session = sessionFactory.openSession();
		Transaction t = session.beginTransaction();
		String stockId = null;
		try {
			for(int i=0 ; i<twHPList.size(); i++) {
				if(i==0) {
					stockId = twHPList.get(0).getId().getStockId();
				    logger.log(Level.INFO, "StockId:"+ stockId +", excelToDatabase, size="+ twHPList.size());
				}
				
				TwHistoricalPrices twHPBean = new TwHistoricalPrices();
				TwHistoricalPricesId twHPBeanId = new TwHistoricalPricesId(); 
				
				twHPBean.setPrice(twHPList.get(i).getPrice());
				twHPBean.setVolume(twHPList.get(i).getVolume());
				twHPBean.setOpen(twHPList.get(i).getOpen());
				twHPBean.setHigh(twHPList.get(i).getHigh());
				twHPBean.setLow(twHPList.get(i).getLow());
			    twHPBean.setPer(twHPList.get(i).getPer());
			    
			    twHPBeanId.setStockId(stockId);
				twHPBeanId.setTradingDate(twHPList.get(i).getId().getTradingDate());
				twHPBean.setId(twHPBeanId);
//			    session.saveOrUpdate(twHPBean);
				session.save(twHPBean);
			}
			t.commit();
			logger.log(Level.INFO, "StockId: " +stockId + ", excelToDatabase success commit");
		} catch (Exception e) {
			logger.log(Level.ERROR, "StockId: " +stockId + "excelToDatabase=>" + e.getMessage());
			t.rollback();
		} finally {
			if(session.isOpen())
				session.close();
		}
		return "success";
	}
	
	public String excelDDEToDatabase(List<TwHistoricalPrices> twHPList) {
		Session session = null;
		Transaction t = null;
		try {
			session = sessionFactory.openSession();
			TwHistoricalPrices twHPBean = null;
			TwHistoricalPricesId twHPBeanId = null; 
			t= session.beginTransaction();
			for(int i=0 ; i<twHPList.size(); i++) {

				twHPBean = new TwHistoricalPrices();
				twHPBeanId = new TwHistoricalPricesId();
				
				//twHPBean.setName(twHPList.get(i).getName());
				twHPBean.setPrice(twHPList.get(i).getPrice());
				twHPBean.setVolume(twHPList.get(i).getVolume());
				twHPBean.setOpen(twHPList.get(i).getOpen());
				twHPBean.setHigh(twHPList.get(i).getHigh());
				twHPBean.setLow(twHPList.get(i).getLow());
				twHPBean.setPriceChangeRatio(twHPList.get(i).getPriceChangeRatio());
			    twHPBean.setPer(twHPList.get(i).getPer());
			    
			    twHPBeanId.setStockId(twHPList.get(i).getId().getStockId());
				twHPBeanId.setTradingDate(twHPList.get(i).getId().getTradingDate());
				twHPBean.setId(twHPBeanId);
				
			    session.save(twHPBean);
//			    session.saveOrUpdate(twHPBean);
			}
			t.commit();
			logger.log(Level.INFO, "excelDDEToDatabase success commit");
		} catch (Exception e) {
			e.printStackTrace();
			logger.log(Level.ERROR,  "DDE insert err=>" + e.getMessage());
			t.rollback();
		} finally {
			if(session.isOpen())
				session.close();
		}
		return "success";
	}
}
