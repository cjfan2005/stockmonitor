package com.gtbc.stock.dao;

import java.util.List;
import java.util.Map;

import com.gtbc.stock.bean.TwHistoricalPrices;


public interface TwHistoricalPricesDAO {
	public List<Map<String, Object>> getStockCondition(String stockId, String name, String tradingDateStr, String tradingDateEnd, String sort, String orderBy);
	public String excelToDatabase(List<TwHistoricalPrices> twHPList);
	public String excelDDEToDatabase(List<TwHistoricalPrices> twHPList);
	public List<Map<String, Object>> getDataForChart(String stockId);
}
