package com.gtbc.stock.dao;

import java.util.List;
import java.util.Map;



public interface TwForceIndexDAO {
	public List<Map<String, Object>> FICallCenter(String stockId, String name);
	public String BestBuyStockCallCenter();
	
	public List<Map<String, Object>> getBestBuyStock();
	public List<Map<String, Object>> getBestBuyShortData(String stockId);
	
	//charts
	public List<Map<String, Object>> getFIData(String stockId, String name, String sort, String orderBy);
	public List<Map<String, Object>> getDataForChart(String stockId);
	
}
