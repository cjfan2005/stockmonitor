package com.gtbc.stock.dao;

import java.util.List;
import java.util.Map;

public interface TwStockOscillatorDAO {
	public List<Map<String, Object>> oscillatorCallCenter(String stockId, int slowEMAParam, int fastEMAParam, int signalParam);

}
