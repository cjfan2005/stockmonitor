package com.gtbc.stock.dao;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.gtbc.stock.bean.TwForceIndex;
import com.gtbc.stock.bean.TwForceIndexId;
import com.gtbc.stock.bean.TwBestBuyStock;
import com.gtbc.stock.bean.TwBestBuyStockId;
import com.gtbc.util.StringUtils;

public class TwForceIndexDAOImpl implements TwForceIndexDAO {
	private static final Logger logger = Logger.getLogger(TwForceIndexDAOImpl.class);
	private SessionFactory sessionFactory;

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	
	public List<Map<String, Object>> getFIData(String stockId, String name, String sort, String orderBy) {
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		Session session = sessionFactory.openSession();
		String sql = "";
		
		try {
			sql += " SELECT ";
			sql += " DATE_FORMAT(hp.TradingDate,'%Y-%m-%d') as tradingDate, ";
			sql += " hp.StockId, sm.Name, hp.Volume, hp.Open, hp.High, hp.Low, hp.Price, ";
			sql += " fi.PriceDiff, fi.Buy1, fi.Buy2, fi.Sale1, fi.Sale2, fi.SumBuy, fi.SumSale, fi.BuyCnt, fi.SaleCnt, ";
			sql += " fi.Day20BuyCnt, fi.Day20SaleCnt, " ;
			sql += " fi.Day14BuyCnt, fi.Day14SaleCnt, " ;
			sql += " fi.Day6BuyCnt, fi.Day6SaleCnt, " ;
			sql += " fi.ForceIndex20, fi.ForceIndex14, fi.ForceIndex6 ";
			sql += " FROM  tw_historical_prices AS hp ";
			sql += " LEFT JOIN tw_stock_map AS sm  ON hp.StockId = sm.StockId ";
			sql += " LEFT JOIN tw_force_index AS fi ON hp.StockId = fi.StockId AND hp.TradingDate = fi.TradingDate ";
			sql += " WHERE ";
			sql += " 1 = 1 ";
			
			
			if(StringUtils.isNotBlank(stockId))
				sql += " AND hp.StockId = '" + stockId + "' ";
			
			if(StringUtils.isNotBlank(name))
				sql += " AND sm.Name = '" + name + "' ";
			
			sql += " ORDER BY hp.tradingDate DESC Limit 500;";
			
			Query query = session.createSQLQuery(sql.toString());
			list = query.list();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session.isOpen())
				session.close();
		}
		return list;
	}
	
	public List<Map<String, Object>> getDataForChart(String stockId) {
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		Session session = sessionFactory.openSession();
		String sql = "";
		
		//'%a %b %d %H:%i:%S +0800 %Y'
		try {
			sql += " SELECT ";
			sql += " DATE_FORMAT(TradingDate,'%a %b %d %H:%i:%S +0800 %Y'), ";
			sql	+= " StockId,SName,Price,Volume,OPEN,High,Low, fi6, fi14, fi20 ";
			sql += " FROM ";
			sql += "   ( ";
			sql += "     SELECT";
			sql	+= "       hp1.TradingDate,hp1.StockId,sm.Name as SName,";
			sql += "       hp1.Price,hp1.Volume,hp1.OPEN,hp1.High,hp1.Low, ";
			sql += "       fi.ForceIndex6 AS fi6,fi.ForceIndex14 AS fi14,fi.ForceIndex20 AS fi20";
			sql += "     FROM ";
			sql += "       tw_historical_prices AS hp1 ";
			sql += "       LEFT JOIN tw_stock_map AS sm ON hp1.StockId = sm.StockId ";
			sql += "       LEFT JOIN tw_force_index AS fi ON hp1.StockId = fi.StockId AND hp1.TradingDate = fi.TradingDate ";
			sql += "     WHERE ";
			sql += "       hp1.StockId = '"+stockId+"'";
			sql += "     ORDER BY ";
			sql += "       hp1.TradingDate DESC";
			sql += "     LIMIT 250";
			sql += "   ) sub" ;
			sql += " ORDER BY TradingDate ASC;"; 
			
			Query query = session.createSQLQuery(sql.toString());
			list = query.list();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session.isOpen())
				session.close();
		}
		return list;
	}
	
	//shows besy Buy Stock on screen
	public List<Map<String, Object>> getBestBuyStock() {
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		Session session = sessionFactory.openSession();
		String sql = "";
		
		try {
			sql += " SELECT ";
			sql += " DATE_FORMAT(bb.AccountingDate,'%Y-%m-%d') as aDate, ";
			sql += " sm.Name, bb.StockId, bb.ForceIndexVal ";
			sql += " FROM  tw_best_buy_stock AS bb ";
			sql += " LEFT JOIN tw_stock_map AS sm  ON bb.StockId = sm.StockId ";
			sql += " WHERE ";
			sql += " 1 = 1 ";
			sql += " ORDER BY bb.ForceIndexVal asc Limit 60;";
			
			Query query = session.createSQLQuery(sql.toString());
			list = query.list();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session.isOpen())
				session.close();
		}
		return list;
	}
	
	//力道指數演算法控制中心
	@SuppressWarnings({ "rawtypes", "unchecked", "unused" })
	public List<Map<String, Object>> FICallCenter(String stockId, String name) {
		
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		Session session = sessionFactory.openSession();
		List<Map<String, Object>> resultList = null;
		
		try {
			//if query condition are null, return empty list.
			if ((stockId == null || stockId.isEmpty()) && (name == null || name.isEmpty())) {
				return null;
			} else {
				
				List stockDetailList = getStockDetail(stockId, name);
				logger.log(Level.INFO, "Begin Alogrithm. StockId: "+ stockId );
				resultList = forceIndexAlgorithm(stockDetailList);
				logger.log(Level.INFO, "End Alogrithm. StockId: "+ stockId);
				
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session.isOpen())
				session.close();
		}
		return resultList;
	}
	
	@SuppressWarnings({ "rawtypes" })
	private List getStockIds() {
		List list = new ArrayList<Map<String, Object>>();
		Session session = sessionFactory.openSession();
		String sql = "";
		try {
			sql += "SELECT DISTINCT(hp1.StockId) FROM tw_historical_prices AS hp1 WHERE ";
            sql += "(SELECT COUNT(hp2.StockId) FROM tw_historical_prices AS hp2 WHERE hp2.StockId = hp1.StockId ) > 240";
            
			Query query = session.createSQLQuery(sql.toString());
			list = query.list();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session.isOpen())
				session.close();
		}
		return list;
	}
	
	@SuppressWarnings({ "unchecked" })
	private List<Map<String, Object>> getStockDetail(String stockId, String name) {
		List<Map<String, Object>> objList =  new ArrayList<Map<String, Object>>();
		List<Map<String, Object>> stockList = new ArrayList<Map<String, Object>>();
		Session session = sessionFactory.openSession();
		String sql = "";
		try {
			
			sql += " SELECT ";
			sql += " TradingDate, ";
			sql += " StockId, Name, Price, Volume, Open, High, Low";
			sql += " FROM (";
			sql += " SELECT ";
			sql += " DATE_FORMAT(hp.TradingDate,'%Y-%m-%d') as tradingDate, "
				+ "  hp.StockId, sm.Name, hp.Price, hp.Volume, hp.Open, hp.High, hp.Low";
			sql += " FROM ";
			sql += " tw_historical_prices AS hp";
			sql += " LEFT JOIN tw_stock_map AS sm ON hp.StockId = sm.StockId ";
			sql += " WHERE 1=1 ";
			
			if(StringUtils.isNotBlank(stockId))
				sql += " AND hp.StockId = '" + stockId + "' ";
			
			if(StringUtils.isNotBlank(name))
				sql += " AND sm.Name = '" + name + "' ";
			
			sql += " ORDER BY hp.tradingDate DESC LIMIT 250 ";
			sql += " ) sub " ;
			sql += " ORDER BY tradingDate ASC;";
            
			Query query = session.createSQLQuery(sql.toString());
			objList = query.list();
			
			if(objList.size()==0 || objList.isEmpty()) {
				session.close();
				return null;
			}
			
			Iterator<?> its = objList.iterator();
			
			Map<String, Object> stockItem;
			while (its.hasNext()) {
				stockItem = new HashMap<String, Object>();

				Object[] obj = (Object[]) its.next();
				
				stockItem.put("TradingDate", obj[0]);
				stockItem.put("StockId", obj[1]);
				stockItem.put("Name", obj[2]);
				stockItem.put("Price", obj[3]);
				stockItem.put("Volume", obj[4]);
				stockItem.put("Open", obj[5]);
				stockItem.put("High", obj[6]);
				stockItem.put("Low", obj[7]);

				stockList.add(stockItem);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session.isOpen())
				session.close();
		}
		return stockList;
	}
	
	private List<Map<String, Object>> forceIndexAlgorithm(List<Map<String, Object>> stockDetailList) {	
		float priceBefore = 0;
		float priceDiff,buy1,buy2,sale1,sale2,sumBuy,sumSale;
		
		List<Map<String, Object>> forceIndexResList = new ArrayList<Map<String, Object>>();
		Map<String, Object> fiItem = null;
		
		Map<Integer, Integer> day6BuyCntMap = new HashMap<Integer, Integer>();
		Map<Integer, Integer> day14BuyCntMap = new HashMap<Integer, Integer>();
		Map<Integer, Integer> day20BuyCntMap = new HashMap<Integer, Integer>();
		
		Map<Integer, Integer> day6SaleCntMap = new HashMap<Integer, Integer>();
		Map<Integer, Integer> day14SaleCntMap = new HashMap<Integer, Integer>();
		Map<Integer, Integer> day20SaleCntMap = new HashMap<Integer, Integer>();
		
		
		try {
			for(int i=0; i<stockDetailList.size(); i++) {
				if(i==0)
				    priceBefore = Float.parseFloat(stockDetailList.get(0).get("Price").toString());
				else {
					String tradingDate = stockDetailList.get(i).get("TradingDate").toString();
					String stockId = stockDetailList.get(i).get("StockId").toString();
					String name = stockDetailList.get(i).get("Name").toString();
					float open = Float.parseFloat(stockDetailList.get(i).get("Open").toString());
					float price = Float.parseFloat(stockDetailList.get(i).get("Price").toString());
					float high = Float.parseFloat(stockDetailList.get(i).get("High").toString());
					float low = Float.parseFloat(stockDetailList.get(i).get("Low").toString());
					int volume = Integer.parseInt(stockDetailList.get(i).get("Volume").toString());
					int day6BuyCnt=0, day6SaleCnt=0, day14BuyCnt=0, day14SaleCnt=0, day20BuyCnt=0, day20SaleCnt=0;
					
				    priceDiff = price - priceBefore;
					
					//buy1
					if(priceDiff > 0) 
						buy1 = Math.abs(open - priceBefore);
					else 
						buy1 = Math.abs(high - open);
					
					//sale1
					if(priceDiff > 0) 
						sale1 = Math.abs(open - low);
					else 
						sale1 = Math.abs(priceBefore - open);
					
					//buy2
					if(priceDiff > 0) 
						buy2= Math.abs(high - low);
					else 
						buy2 = Math.abs(price - low);
					
					//Sale2
					if(priceDiff > 0) 
						sale2 = Math.abs(high - price);
					else 
						sale2 = Math.abs(high - low);
					
					priceBefore = price;
					//SumBuy & SumSale [漲/跌幅總合]
					sumBuy = buy1 + buy2; sumSale = sale1 + sale2;
					
					//BuyCnt & SaleCnt [買/賣力道張數]
					int buyCnt = (int) ((volume)*(sumBuy/(sumBuy+sumSale)));
					int saleCnt = (int) ((volume)*(sumSale/(sumBuy+sumSale)));
					double forceIndex6=0, forceIndex14=0, forceIndex20=0;
					
					//Day6BuyCnt & Day6SaleCnt & FI [06天總買/賣盤/FI6]
					day6BuyCntMap.put(i, buyCnt);
					day6SaleCntMap.put(i,  saleCnt);
					if(i>5) {
						int x=0;
						for(x=i; x>i-6; x--) {
							day6BuyCnt += day6BuyCntMap.get(x);
							day6SaleCnt += day6SaleCntMap.get(x);
						}
						forceIndex6 = (double)day6SaleCnt/(double)day6BuyCnt;
						
						BigDecimal bd= new BigDecimal(forceIndex6);   
					    bd=bd.setScale(4, BigDecimal.ROUND_HALF_UP);//小數後面4位, 四捨五入
					    forceIndex6 = bd.doubleValue();
					}
					
			        //Day14BuyCnt & Day14SaleCnt & FI [14天總買/賣盤/FI14]
					day14BuyCntMap.put(i, buyCnt);
					day14SaleCntMap.put(i,  saleCnt);
					if(i>13) {
						int x=0;
						for(x=i; x>i-14; x--) {
							day14BuyCnt += day14BuyCntMap.get(x);
							day14SaleCnt += day14SaleCntMap.get(x);
						}
						forceIndex14 = (double)day14SaleCnt/(double)day14BuyCnt;
						
						BigDecimal bd= new BigDecimal(forceIndex14);   
					    bd=bd.setScale(4, BigDecimal.ROUND_HALF_UP);//小數後面4位, 四捨五入
					    forceIndex14 = bd.doubleValue();
					}
					
					//Day20BuyCnt & Day20SaleCnt & FI [20天總買/賣盤/FI20]
					day20BuyCntMap.put(i, buyCnt);
					day20SaleCntMap.put(i,  saleCnt);
					if(i>19) {
						int x=0;
						for(x=i; x>i-20; x--) {
							day20BuyCnt += day20BuyCntMap.get(x);
							day20SaleCnt += day20SaleCntMap.get(x);
						}
						forceIndex20 = (double)day20SaleCnt/(double)day20BuyCnt;
						
						BigDecimal bd= new BigDecimal(forceIndex20);   
					    bd=bd.setScale(4, BigDecimal.ROUND_HALF_UP);//小數後面4位, 四捨五入
					    forceIndex20 = bd.doubleValue();
					}
					
					//fiItem
					fiItem = new HashMap<String, Object>();
					fiItem.put("TradingDate", tradingDate);
					fiItem.put("StockId", stockId);
					fiItem.put("Name", name);
					fiItem.put("Open", open);
					fiItem.put("High", high);
					fiItem.put("Low", low);
					fiItem.put("Close", price);
					fiItem.put("Volume", volume);
					fiItem.put("ForceIndex20", forceIndex20);
					fiItem.put("ForceIndex14", forceIndex14);
					fiItem.put("ForceIndex6", forceIndex6);
					forceIndexResList.add(fiItem);
			    }
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.log(Level.INFO, "Force Index occured Err:" + e.getMessage());
		}
		return forceIndexResList;
	}
	
	//僅保留最近10個月fi資料
	@SuppressWarnings("unused")
	private String deleteOldFIData() {
		String tradingDate;
		Calendar c = Calendar.getInstance();
		c.setTime(new Date());
		c.add(Calendar.MONTH, -10);
		tradingDate = new SimpleDateFormat("yyyy/MM/dd").format(c.getTime());

		tradingDate = tradingDate.replace("/", "-") ;//+ " 23:59:59";
		logger.log(Level.INFO, "Delete old FI from : "+ tradingDate);
		
		
		Session session = null;
		String sql = "DELETE FROM tw_force_index WHERE TradingDate <= '"+ tradingDate + "' ";
		try {
			session = sessionFactory.openSession();
			Transaction t = session.beginTransaction();
			session.createSQLQuery(sql).executeUpdate();
			t.commit();
		} catch (HibernateException e) {
			e.printStackTrace();
		}finally{
			session.close();
		}
		return "success";
	}
	
	//Best Buy Stock 
	@SuppressWarnings("unchecked")
	public String BestBuyStockCallCenter() {
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		Session session = sessionFactory.openSession();
		String algorithmResult = "fail";
		
		List stockIdList = getStockIds();
		try {
			if(stockIdList.size()==0 || stockIdList.isEmpty()) {
				return null;
			}
			
			for(int i=0; i<stockIdList.size(); i++) {
				String stockId = stockIdList.get(i).toString();
				List stockDetailList = getBestBuyStockDetail(stockId);
				logger.log(Level.INFO, "Begin BestBuy Alogrithm. StockId: "+ stockId + ", Size= " + stockDetailList.size());
				algorithmResult = bestBuyAlgorithm(stockDetailList);
				logger.log(Level.INFO, "End BestBuy Alogrithm. StockId: "+ stockId);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session.isOpen())
				session.close();
		}
		return algorithmResult;
	}
	
	//best buy stocks中點選特定股票,僅撈少量資料回頁面即可(5-10筆,short data is fine)
	public List<Map<String, Object>> getBestBuyShortData(String stockId) {
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		Session session = sessionFactory.openSession();
		String sql = "";
		
		try {
			sql += " SELECT ";
			sql += " DATE_FORMAT(hp.TradingDate,'%m/%d') as tradingDate, ";
			sql += " hp.Open, hp.High, hp.Low, hp.Price, hp.Volume, ";
			sql += " fi.ForceIndex6, fi.ForceIndex14, fi.ForceIndex20 ";
			sql += " FROM  tw_historical_prices AS hp ";
			sql += " LEFT JOIN tw_force_index AS fi ON hp.StockId = fi.StockId AND hp.TradingDate = fi.TradingDate ";
			sql += " WHERE ";
		    sql += " hp.StockId = '" + stockId + "' ";
			sql += " ORDER BY hp.tradingDate DESC Limit 5;";
			
			Query query = session.createSQLQuery(sql.toString());
			list = query.list();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session.isOpen())
				session.close();
		}
		return list; 
	}
	
	private String bestBuyAlgorithm(List<Map<String, Object>> stockDetailList) throws ParseException {
		Session session = sessionFactory.openSession();
		Transaction t = session.beginTransaction();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		double fiDayAvg = 0, forceIndex6 = 0, forceIndex14 = 0, forceIndex20 = 0;
		String accountingDate = null, stockId = null ;
		int stockDetailListSize = stockDetailList.size();
		try {
			for(int i=0; i<stockDetailList.size(); i++) {
				if(i==0) {
					accountingDate = stockDetailList.get(i).get("AccountingDate").toString();
					stockId = stockDetailList.get(i).get("StockId").toString();
				}
				forceIndex6 += Double.parseDouble(stockDetailList.get(i).get("ForceIndex6").toString());
				forceIndex14 += Double.parseDouble(stockDetailList.get(i).get("ForceIndex14").toString());
				forceIndex20 += Double.parseDouble(stockDetailList.get(i).get("ForceIndex20").toString());
				
			}
			forceIndex6 = ((double)forceIndex6/(double)stockDetailListSize)*10.0;//加權10%比重
			forceIndex14 = ((double)forceIndex14/(double)stockDetailListSize)*10.0;//加權10%比重
			forceIndex20 = ((double)forceIndex20/(double)stockDetailListSize)*80.0;//加權80%比重
			
			fiDayAvg = (forceIndex6 + forceIndex14 + forceIndex20)/100.0;
			
			if((fiDayAvg<0.8 && fiDayAvg>0.2) && (forceIndex20/80.0)<0.9) {
				TwBestBuyStock bbBean = new TwBestBuyStock();
				TwBestBuyStockId bbBeanId = new TwBestBuyStockId();
				
				bbBean.setForceIndexVal(fiDayAvg);
				
				bbBeanId.setAccountingDate(new Date());
				bbBeanId.setStockId(stockId);
				bbBean.setId(bbBeanId);
				session.save(bbBean);
				t.commit();
			}
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if(session.isOpen())
				session.close();
		}
		return null;
	}
	
	private List<Map<String, Object>> getBestBuyStockDetail(String stockId) {
		List<Map<String, Object>> objList =  new ArrayList<Map<String, Object>>();
		List<Map<String, Object>> stockList = new ArrayList<Map<String, Object>>();
		Session session = sessionFactory.openSession();
		String sql = "";
		try {
			
			sql += " SELECT ";
			sql += " DATE_FORMAT(fi.TradingDate,'%Y-%m-%d') as tradingDate, ";
			sql	+= " fi.StockId, fi.ForceIndex6, fi.ForceIndex14, fi.ForceIndex20 ";
			sql += " FROM ";
			sql += " tw_force_index AS fi";
			sql += " WHERE ";
			sql += " fi.StockId = '" + stockId + "' ";
			//只算最新的20筆資料。
			sql += " ORDER BY fi.tradingDate DESC LIMIT 20 ";
            
			Query query = session.createSQLQuery(sql.toString());
			objList = query.list();
			
			if(objList.size()==0 || objList.isEmpty()) {
				session.close();
				return null;
			}
			
			Iterator<?> its = objList.iterator();
			
			Map<String, Object> stockItem;
			while (its.hasNext()) {
				stockItem = new HashMap<String, Object>();

				Object[] obj = (Object[]) its.next();
				
				stockItem.put("AccountingDate", obj[0]);
				stockItem.put("StockId", obj[1]);
				stockItem.put("ForceIndex6", obj[2]);
				stockItem.put("ForceIndex14", obj[3]);
				stockItem.put("ForceIndex20", obj[4]);

				stockList.add(stockItem);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session.isOpen())
				session.close();
		}
		return stockList;
	}
}
