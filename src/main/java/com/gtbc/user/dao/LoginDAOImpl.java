package com.gtbc.user.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.struts2.ServletActionContext;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import com.gtbc.user.bean.LoginInfo;

public class LoginDAOImpl implements LoginDAO {
	private SessionFactory sessionFactory;
	public void setSessionFactory(SessionFactory sessionFactory){
        this.sessionFactory = sessionFactory;
    }
	
	@SuppressWarnings({ "unchecked", "unused" })
	public String validateUser(LoginInfo loginInfo){
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		
		String sql = "";
		try {
			sql += " SELECT ";
			sql += " u.userId, u.password, u.name, u.email, u.mobile, u.deleteFlag, u.note, ";
			sql += " cm.Note1,"; //role
			sql += " cm2.Note2,";  //notifyGroup
			sql += " u.createDate, ";
			sql += " u.role , ";
			sql += " cm2.Note3 "; //Raw of the NotifyGroup.
			sql += " FROM users AS u ";
			sql += " LEFT JOIN code_map AS cm  ON cm.CodeSequence = u.role ";
			sql += " LEFT JOIN code_map AS cm2 ON cm2.CodeSequence = u.notifyGroup ";
			sql += " WHERE u.userId = '" + loginInfo.getUserId() + "' ";
			sql += " AND u.password = '" + loginInfo.getPassword() + "' ";
			sql += " AND u.deleteFlag = 'N';";
			Query query = session.createSQLQuery(sql);
			list =  query.list();
			
			if ((list == null) || (list.size()==0)) {
            	ServletActionContext.getRequest().getSession().setAttribute("loggedInRole", "GUEST");
            	//auditDao.createAudit(session, AuditUtils.getAudit(loginInfo, AuditUtils.LOGIN, AuditType.Login,"validateUser", sql, "Login Fail"));
            	//transaction.commit();
            	return "input";
			} else {
				//資料整理
				Iterator<?> its = list.iterator();

				List<Map<String, Object>> usersList = new ArrayList<Map<String, Object>>();
				Map<String, Object> users;

				while (its.hasNext()) {
					users = new HashMap<String, Object>();

					Object[] obj = (Object[]) its.next();

					users.put("userId", obj[0]);
					users.put("password", obj[1]);
					users.put("name", obj[2]);
					users.put("email", obj[3]);
					users.put("mobile", obj[4]);
					users.put("deleteFlag", obj[5]);
					users.put("note", obj[6]);
					users.put("role", obj[7]);
					users.put("notifyGroup", obj[8]);
					users.put("createDate", obj[9]);
					users.put("rawRole", obj[10]);
					users.put("rawNotifyGroup", obj[11]);

					usersList.add(users);
				}
				
				//寫入Audit Log
				ServletActionContext.getRequest().getSession().setAttribute("loggedInRole", usersList.get(0).get("role"));
				ServletActionContext.getRequest().getSession().setAttribute("loggedNotifyGroup", usersList.get(0).get("notifyGroup"));
				ServletActionContext.getRequest().getSession().setAttribute("loggedInRawRole", usersList.get(0).get("rawRole"));
				ServletActionContext.getRequest().getSession().setAttribute("loggedRawNotifyGroup", usersList.get(0).get("rawNotifyGroup"));
	           
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session.isOpen()){
				
				session.close();
			}
				
		}
		return "success";
	}
	
}
