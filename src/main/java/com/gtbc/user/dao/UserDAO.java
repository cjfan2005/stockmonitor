package com.gtbc.user.dao;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.gtbc.user.bean.*;

public interface UserDAO {
	public String modifyUser(Users user, String modifyUserAct);
	public List<Users> getAllUser(String deleteFlag);
	public List<Map<String, Object>> getUserCondition(Users u, Date createdateBgn, Date createdateEnd, String sort, String orderBy);
}