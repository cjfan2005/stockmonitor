package com.gtbc.user.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import com.gtbc.user.bean.Users;
import com.gtbc.util.StringUtils;

public class UserDAOImpl implements UserDAO {
	private SessionFactory sessionFactory;

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	public List<Users> getAllUser(String deleteFlag) {
		Session session = null;
		session = sessionFactory.openSession();
		
		Criteria c = session.createCriteria(Users.class);
		if (deleteFlag != null && !deleteFlag.isEmpty())
			c.add(Restrictions.eq("deleteFlag", deleteFlag));
		List<Users> uList = c.list();
		session.close();
		return uList;
	}
	
	/**
	 * 點選『DELETE』,deleteFlag改為"Y".
	 */
	public String deleteUser(String userId) {
		Session session = null;
		String sql = "UPDATE users SET deleteFlag='Y' WHERE userId=" + userId + "";
		try {
			session = sessionFactory.openSession();
			Transaction t = session.beginTransaction();
			session.createSQLQuery(sql).executeUpdate();
			t.commit();
		} catch (HibernateException e) {
			e.printStackTrace();
		}finally{
			session.close();
		}
		return "success";
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> getUserCondition(Users u, Date createDateBgn, Date createDateEnd, String sort, String orderBy) {
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		Session session = sessionFactory.openSession();
		String sql = "";
		try {
			sql += " SELECT ";
			sql += " u.userId, u.password, u.name, u.email, u.mobile, u.deleteFlag, u.note, ";
			sql += " cm.Note1,"; //role
			sql += " cm2.Note2,";       //
			sql += " u.createDate ";
			sql += " FROM users AS u ";
			sql += " LEFT JOIN code_map AS cm  ON cm.CodeSequence = u.role ";
			sql += " LEFT JOIN code_map AS cm2 ON cm2.CodeSequence = u.notifyGroup ";
			sql += " WHERE 1=1 ";
			
			if(StringUtils.isNotBlank(u.getUserId()))
				sql += " AND u.userId = '" + u.getUserId() + "' ";
			
			if(StringUtils.isNotBlank(u.getDeleteFlag()))
				sql += " AND u.deleteFlag = '" + u.getDeleteFlag() + "' ";
			
			if(StringUtils.isNotBlank(orderBy) && StringUtils.isNotBlank(sort)){
				sql += " ORDER BY ";
				sql += sort + " " + orderBy;
			}else{
				sql += " ORDER BY ";
				sql += " u.userId ASC ";
			}
			
			Query query = session.createSQLQuery(sql);
			list = query.list();
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session.isOpen())
				session.close();
		}
		return list;
	}
	

	public List<Users> getUserCondition(Users u, Date createdateBgn, Date createdateEnd){
		Session session = null;
		session = sessionFactory.openSession();
		Criteria c = session.createCriteria(Users.class);
		
		String userId = u.getUserId();
		String deleteFlag = u.getDeleteFlag();
		
		if (userId != null && !userId.isEmpty())
			c.add(Restrictions.eq("userId", userId));
		if (deleteFlag != null && !deleteFlag.isEmpty())
			c.add(Restrictions.eq("deleteFlag", deleteFlag));
		if (createdateBgn != null && createdateEnd != null)
		    c.add(Restrictions.between("createdate", createdateBgn, createdateEnd));
		
		List<Users> uList = c.list();
		session.close();
		return uList;
	}
	
	
	public String modifyUser(Users user, String modifyUserAct) {
    	Session session = null;
    	try {
    		session = sessionFactory.openSession();
    		Transaction t = session.beginTransaction();
    		String strSql = "";
    		if(modifyUserAct.equalsIgnoreCase("add")) {

    			session.save(user);
    			t.commit();
    			return "success";
    			
    		} else if(modifyUserAct.equalsIgnoreCase("modify")) {
    			strSql = " UPDATE users " +
    					" SET role= '"+ user.getRole() +"' "+
    					" 	, mobile= '"+ user.getMobile() +"' "+
    					" 	, email= '"+ user.getEmail() +"' "+
    					"	, notifyGroup= '"+ user.getNotifyGroup() +"' "+
						" 	, note= '"+ user.getNote() +"' "+
						" WHERE userId= '"+ user.getUserId() +"' ";
    			
    		} else if (modifyUserAct.equalsIgnoreCase("delete") && !user.getUserId().equalsIgnoreCase("admin")) {
    			strSql = "UPDATE Users SET deleteFlag='Y' WHERE userId='" + user.getUserId() + "'";
    			
    		} else if (modifyUserAct.equalsIgnoreCase("recovery") && !user.getUserId().equalsIgnoreCase("admin")) {
    			strSql = " UPDATE users " +
    					" SET role= '"+ user.getRole() +"' "+
    					" , mobile= '"+ user.getMobile() +"' "+
    					" , email= '"+ user.getEmail() +"' "+
    					" , notifyGroup= '"+ user.getNotifyGroup() +"' "+
						" , note= '"+ user.getNote() +"' "+
						" , deleteFlag= '"+ user.getDeleteFlag() +"' "+
						" WHERE userId= '"+ user.getUserId() +"' ";
    		} else {
    			
    			return "success";
    		}
	    	
			session.createSQLQuery(strSql).executeUpdate();
			t.commit();
	    	
	    	
    	} catch (Exception e) {
			e.printStackTrace();
		} finally{
    		if (session.isOpen()) {
        		session.close();
        	} 
        }
    	return "success";
	}

}
