package com.gtbc.user.dao;

import com.gtbc.user.bean.LoginInfo;

public interface LoginDAO {

	//public String validateUser(String userId, String password);
	public String validateUser(LoginInfo loginInfo);
}
