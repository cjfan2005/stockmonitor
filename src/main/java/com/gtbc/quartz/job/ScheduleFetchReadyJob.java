package com.gtbc.quartz.job;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import com.gtbc.stock.dao.TwForceIndexDAO;

/**
 * 
 * @author kent
 * Execute every 50 minutes.
 */
public class ScheduleFetchReadyJob implements Job {
	
	private static final Logger logger = Logger.getLogger(ScheduleFetchReadyJob.class);

	public void execute(JobExecutionContext context) throws JobExecutionException {
		logger.log(Level.INFO, "SCHEDULE: "
				+ "<ScheduleFetchReadyJob> activated at: <"+ context.getFireTime().toString() +">"
				+ ", next scheduled firing time: <"+ context.getNextFireTime().toString() + ">");

		TwForceIndexDAO twFIDAO = null;

		boolean isRun = true;
//		boolean isRun = false;
		
		try {
			twFIDAO = (TwForceIndexDAO) context.getScheduler().getContext().get("twForceIndexDAO");
		}catch (Exception e){
			logger.log(Level.ERROR, "Could not load required services, stopping task...");
			isRun = false;
		}
		
		if (isRun) {
			String algorithmResult ="success";//= twFIDAO.FICallCenter();
			if(algorithmResult.equals("success")) {
				logger.log(Level.INFO, "Completed ForceIndex Algorithm.");
			} else {
				logger.log(Level.ERROR, "Failed ForceIndex Algorithm.");
			}
		}
	}
}
