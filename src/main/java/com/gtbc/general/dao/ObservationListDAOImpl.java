package com.gtbc.general.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.gtbc.util.StringUtils;

public class ObservationListDAOImpl implements ObservationListDAO {
	private static final Logger logger = Logger.getLogger(ObservationListDAOImpl.class);
	private SessionFactory sessionFactory;

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	/*
	  ItemId = StockId or FutureId.
	 */
	
	public List<Map<String, Object>> getObservationItemId() {
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		List<Map<String, Object>> oiList = null;
		Session session = sessionFactory.openSession();
		String sql = "";
		
		HttpServletRequest req = ServletActionContext.getRequest();
		String userId = (String) req.getSession().getAttribute("loggedInUser");
		
		try {
			sql += " SELECT ";
			sql += " sm.Name, oi.ItemId ";
			sql += " FROM observation_item AS oi ";
			sql += " LEFT JOIN tw_stock_map AS sm ON sm.StockId = oi.ItemId";
			sql += " WHERE";
			sql += " oi.UserId = '"+userId+"' ";
			sql += " ORDER BY oi.ItemId ASC;";
			
			Query query = session.createSQLQuery(sql.toString());
			list = query.list();
			
			Iterator<?> its = list.iterator();

			oiList = new ArrayList<Map<String, Object>>();
			Map<String, Object> stockItem;
			
			while (its.hasNext()) {
				stockItem = new HashMap<String, Object>();

				Object[] obj = (Object[]) its.next();
				stockItem.put("ItemName", obj[0]);
				stockItem.put("ItemId", obj[1]);

				oiList.add(stockItem);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session.isOpen())
				session.close();
		}
		return oiList; 
	}
	
	
	//觀察名單(Observation list)中點選特定股票,僅撈少量資料回頁面即可(5-10筆,short data is fine)
	public List<Map<String, Object>> getItemIdShortData(String itemId) {
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		Session session = sessionFactory.openSession();
		String sql = "";
		
		try {
			sql += " SELECT ";
			sql += " DATE_FORMAT(hp.TradingDate,'%m/%d') as tradingDate, ";
			sql += " hp.Open, hp.High, hp.Low, hp.Price, hp.Volume ";
			sql += " FROM  tw_historical_prices AS hp ";
			sql += " WHERE ";
		    sql += " hp.StockId = '" + itemId + "' ";
			sql += " ORDER BY hp.tradingDate DESC Limit 5;";
			
			Query query = session.createSQLQuery(sql.toString());
			list = query.list();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session.isOpen())
				session.close();
		}
		return list; 
	}
	
	public List<Map<String, Object>> observationListData() {
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		List<Map<String, Object>> oiList = null;
		Session session = sessionFactory.openSession();
		String sql = "";
		
		HttpServletRequest req = ServletActionContext.getRequest();
		String userId = (String) req.getSession().getAttribute("loggedInUser");
		
		try {
			sql += " SELECT ";
			sql += " DATE_FORMAT(hp.TradingDate,'%Y-%m-%d') as tradingDate, ";
			sql += " sm.Name, sm.StockId,";
			sql += " hp.Open, hp.High, hp.Low, hp.Price, hp.Volume";
			sql += " FROM observation_item AS oi ";
			sql += " LEFT JOIN tw_historical_prices AS hp ON hp.StockId = oi.ItemId ";
			sql += " LEFT JOIN tw_stock_map AS sm ON sm.StockId = oi.ItemId";
			sql += " WHERE";
			sql += " oi.UserId = '"+userId+"' ";
			sql += " ORDER BY oi.ItemId ASC;";
			
			Query query = session.createSQLQuery(sql.toString());
			list = query.list();
			
			Iterator<?> its = list.iterator();

			oiList = new ArrayList<Map<String, Object>>();
			Map<String, Object> stockItem;
			
			while (its.hasNext()) {
				stockItem = new HashMap<String, Object>();

				Object[] obj = (Object[]) its.next();
				
				stockItem.put("TradingDate", obj[0]);
				stockItem.put("Name", obj[1]);
				stockItem.put("StockId", obj[2]);
				stockItem.put("Open", obj[3]);
				stockItem.put("High", obj[4]);
				stockItem.put("Low", obj[5]);
				stockItem.put("Price", obj[6]);
				stockItem.put("Volume", obj[7]);

				oiList.add(stockItem);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session.isOpen())
				session.close();
		}
		return oiList; 
	}
	
	@SuppressWarnings({ "unchecked" })
	public List<Map<String, Object>> getItemDataForCharts(String itemId) {
		List<Map<String, Object>> objList =  new ArrayList<Map<String, Object>>();
		List<Map<String, Object>> itemList = new ArrayList<Map<String, Object>>();
		List<Map<String, Object>> combimeDataResult = null;
		
		Session session = sessionFactory.openSession();
		String sql = "";
		try {
			
			sql += " SELECT ";
			sql += " TdUnixTime, TradingDate, ";
			sql += " ItemId, ItemName, Price, Volume, Open, High, Low ";
			sql += " FROM (";
			sql += "   SELECT ";
			sql += "   UNIX_TIMESTAMP(hp.TradingDate) as tdUnixTime, ";
			sql += "   DATE_FORMAT(hp.TradingDate,'%Y-%m-%d') as tradingDate, ";
			sql	+= "   hp.StockId as ItemId, sm.Name as ItemName, hp.Price, hp.Volume, hp.Open, hp.High, hp.Low ";
			sql += "   FROM ";
			sql += "   tw_historical_prices AS hp";
			sql += "   LEFT JOIN tw_stock_map AS sm  ON hp.StockId = sm.StockId ";
			sql += "   WHERE ";
			sql += "   hp.StockId = '" + itemId + "' ";
			sql += "   ORDER BY hp.tradingDate DESC LIMIT 108";
			sql += " ) sub " ;
			sql += " ORDER BY tradingDate ASC;";

			Query query = session.createSQLQuery(sql.toString());
			objList = query.list();
			
			//ARRANGE DATA
			if(objList.size()==0 || objList.isEmpty()) {
				session.close();
				return null;
			}
			
			Iterator<?> its = objList.iterator();
			
			Map<String, Object> stockItem;
			while (its.hasNext()) {
				stockItem = new HashMap<String, Object>();

				Object[] obj = (Object[]) its.next();
				
				stockItem.put("TdUnixTime", obj[0]);
				stockItem.put("TradingDate", obj[1]);
				stockItem.put("ItemId", obj[2]);
				stockItem.put("ItemName", obj[3]);
				stockItem.put("Price", obj[4]);
				stockItem.put("Volume", obj[5]);
				stockItem.put("Open", obj[6]);
				stockItem.put("High", obj[7]);
				stockItem.put("Low", obj[8]);
                itemList.add(stockItem);
			}
			
			//Oscillation algorithm & assemble necessary data for 2 charts
			combimeDataResult = oscillatorAlgorithm(itemId, itemList, 26, 12, 9);
			
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session.isOpen())
				session.close();
		}
		return combimeDataResult;
	}
	
	//TwStockOscillatorDAOImpl.java有相同的方法,若羅機有更改,務必同步二地。
	private List<Map<String, Object>> oscillatorAlgorithm(String stockId, List<Map<String, Object>> stockDetailList, int slowEMAParam, int fastEMAParam, int signalParam) {

		List<Map<String, Object>> oscillatorResList = new ArrayList<Map<String, Object>>();

		double firstSlowEMA=0, firstFastEMA=0, firstSignal=0; 
		double close=0, slowEMA=0, fastEMA=0, macd=0, signal=0, histogramVal=0;
		int signalParamCounter=0;
		
		Map<String, Object> chartItem = null;
		Map<Integer, Double> closeMap = new HashMap<Integer, Double>();
		Map<Integer, Double> slowEMAMap = new HashMap<Integer, Double>();
		Map<Integer, Double> fastEMAMap = new HashMap<Integer, Double>();
		Map<Integer, Double> macdMap = new HashMap<Integer, Double>();
		Map<Integer, Double> signalMap = new HashMap<Integer, Double>();
		Map<Integer, Double> histogramMap = new HashMap<Integer, Double>();
		Map<Integer, Integer> conHisMap = new HashMap<Integer, Integer>();
		
		for(int i=0; i<stockDetailList.size(); i++) {
			try {
				chartItem = new HashMap<String, Object>();
				
				chartItem.put("ItemName", stockDetailList.get(i).get("ItemName"));
				chartItem.put("ItemId", stockId);
				chartItem.put("TdUnixTime", stockDetailList.get(i).get("TdUnixTime"));
				chartItem.put("TradingDate", stockDetailList.get(i).get("TradingDate"));
				chartItem.put("Open", stockDetailList.get(i).get("Open"));
				chartItem.put("High", stockDetailList.get(i).get("High"));
				chartItem.put("Low", stockDetailList.get(i).get("Low"));
				chartItem.put("Volume", stockDetailList.get(i).get("Volume"));
				
				close = Double.parseDouble(stockDetailList.get(i).get("Price").toString());//收盤價
				chartItem.put("Close", close);
				
				closeMap.put(i, close);//收盤價map
				
				//First Fast EMA value
				if(i==(fastEMAParam-1)) {
					double closeTemp = 0;
					for(int x=0; x<fastEMAParam; x++) {
						closeTemp += closeMap.get(x);
					}
					firstFastEMA = closeTemp/fastEMAParam;
					
					BigDecimal bd= new BigDecimal(firstFastEMA);   
				    bd=bd.setScale(4, BigDecimal.ROUND_HALF_UP);//小數後面4位, 四捨五入   
					
					fastEMAMap.put(i, bd.doubleValue());
					chartItem.put("FastEMA", bd.doubleValue());
					
				} else if (i>(fastEMAParam-1)) {
					fastEMA = (close*(2.0/(fastEMAParam+1.0))) + (fastEMAMap.get(i-1))*(1.0-(2.0/(fastEMAParam+1)));
					
					BigDecimal bd= new BigDecimal(fastEMA);   
				    bd=bd.setScale(4, BigDecimal.ROUND_HALF_UP);//小數後面4位, 四捨五入  
					
					fastEMAMap.put(i, bd.doubleValue());
					chartItem.put("FastEMA", bd.doubleValue());
				}
					
				//First Slow EMA value
				if(i==(slowEMAParam-1)) {
					double closeTemp = 0;
					for(int x=0; x<slowEMAParam; x++) {
						closeTemp += closeMap.get(x);
					}
					firstSlowEMA = closeTemp/slowEMAParam;
					
					BigDecimal bd= new BigDecimal(firstSlowEMA);   
				    bd=bd.setScale(3, BigDecimal.ROUND_HALF_UP);//小數後面3位, 四捨五入  
					
					slowEMAMap.put(i, bd.doubleValue());
					chartItem.put("SlowEMA", bd.doubleValue());
					
				} else if (i>(slowEMAParam-1)) {
					
					slowEMA = (close*(2.0/(slowEMAParam+1.0))) + (slowEMAMap.get(i-1))*(1.0-(2.0/(slowEMAParam+1.0)));
					
					BigDecimal bd= new BigDecimal(slowEMA);   
				    bd=bd.setScale(3, BigDecimal.ROUND_HALF_UP);//小數後面3位, 四捨五入  
					
					slowEMAMap.put(i, bd.doubleValue());
					chartItem.put("SlowEMA", bd.doubleValue());
				}
				
				//MACD Map value 
				if(i>=(slowEMAParam-1)) {
					macd = fastEMAMap.get(i) - slowEMAMap.get(i);
					
					BigDecimal bd= new BigDecimal(macd);   
				    bd=bd.setScale(4, BigDecimal.ROUND_HALF_UP);//小數後面4位, 四捨五入  
					
					macdMap.put(i, bd.doubleValue());
					chartItem.put("MACD", bd.doubleValue());
					signalParamCounter++;
				}
				
				//First signal value
				if(signalParamCounter==signalParam) {
					double macdTemp = 0;
					for(int x=i; x>(i-signalParam); x--) {
						macdTemp += macdMap.get(x);
					}
					firstSignal = macdTemp/signalParam;
					
					BigDecimal bd= new BigDecimal(firstSignal);   
				    bd=bd.setScale(4, BigDecimal.ROUND_HALF_UP);//小數後面4位, 四捨五入  
					
					signalMap.put(i, bd.doubleValue());
					chartItem.put("Signal", bd.doubleValue());
				} else if(signalParamCounter>signalParam) {
					
					signal = macd*(2.0/(signalParam+1.0)) + (signalMap.get(i-1)*(1.0-(2.0/(signalParam+1.0))));
					
					BigDecimal bd= new BigDecimal(signal);   
				    bd=bd.setScale(4, BigDecimal.ROUND_HALF_UP);//小數後面4位, 四捨五入  
					
					signalMap.put(i, bd.doubleValue());
					chartItem.put("Signal", bd.doubleValue());
				}
				
				//Histogram Map 
				if(signalParamCounter>=(signalParam)) {
					
					histogramVal = macdMap.get(i) - signalMap.get(i);
					
					BigDecimal bd= new BigDecimal(histogramVal);   
				    bd=bd.setScale(2, BigDecimal.ROUND_HALF_UP);//小數後面2位, 四捨五入   
					
					histogramMap.put(i, bd.doubleValue());
					chartItem.put("HistogramVal", bd.doubleValue());
					
					//ConHisgramMap value
					if(signalParamCounter>signalParam){
						int conHisVal=0;
						Double obj1 = new Double(histogramMap.get(i).toString());
					    Double obj2 = new Double(histogramMap.get(i-1).toString());
					    int retval =  obj1.compareTo(obj2);
						
						if(retval>0)  
							conHisVal=1;
						else 
							conHisVal=0;
						
						conHisMap.put(i, conHisVal);
						chartItem.put("ConHistoVal", conHisVal);
					}
				} else {
					chartItem.put("HistogramVal", 0);
				}
				
				//MA5
				if(i>=4) {
					int x=0; double ma5Sum = 0, ma5 = 0;
					for(x=i; x>i-5; x--) {
						ma5Sum += closeMap.get(x);
					}
					ma5 = (double)ma5Sum/5.0;
					
					BigDecimal bd= new BigDecimal(ma5);   
				    bd=bd.setScale(3, BigDecimal.ROUND_HALF_UP);//小數後面3位, 四捨五入
				    ma5 = bd.doubleValue();
				    //ma5Map.put(i, ma5);
				    chartItem.put("MA5", ma5);
				} else {
					chartItem.put("MA5", closeMap.get(i));//勿參考,為了圖形美觀塞的值
				}
				
				//ma10
				if(i>=9) {
					int x=0; double ma10Sum = 0, ma10 = 0;
					for(x=i; x>i-10; x--) {
						ma10Sum += closeMap.get(x);
					}
					ma10 = (double)ma10Sum/10.0;
					
					BigDecimal bd= new BigDecimal(ma10);   
				    bd=bd.setScale(3, BigDecimal.ROUND_HALF_UP);//小數後面3位, 四捨五入
				    ma10 = bd.doubleValue();
				    //ma5Map.put(i, ma5);
				    chartItem.put("MA10", ma10);
				} else {
					chartItem.put("MA10", closeMap.get(i));//勿參考,為了圖形美觀塞的值
				}
				
				//ma20
				if(i>=19) {
					int x=0; double ma20Sum = 0, ma20 = 0;
					for(x=i; x>i-20; x--) {
						ma20Sum += closeMap.get(x);
					}
					ma20 = (double)ma20Sum/20.0;
					
					BigDecimal bd= new BigDecimal(ma20);   
				    bd=bd.setScale(3, BigDecimal.ROUND_HALF_UP);//小數後面3位, 四捨五入
				    ma20 = bd.doubleValue();
				    //ma5Map.put(i, ma5);
				    chartItem.put("MA20", ma20);
				} else {
					chartItem.put("MA20", closeMap.get(i));//勿參考,為了圖形美觀塞的值
				}
				
				//summary to oscillatorResList 
				oscillatorResList.add(chartItem);
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return oscillatorResList;
	}
}
