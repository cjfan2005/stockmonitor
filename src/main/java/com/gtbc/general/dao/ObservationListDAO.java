package com.gtbc.general.dao;

import java.util.List;
import java.util.Map;

public interface ObservationListDAO {
	public List<Map<String, Object>> getObservationItemId();
	public List<Map<String, Object>> getItemIdShortData(String itemId);
	public List<Map<String, Object>> getItemDataForCharts(String itemId);
	public List<Map<String, Object>> observationListData();//unused
}
