package com.gtbc.general.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class JsonFormatDAOImpl implements JsonFormatDAO {
	private static final Logger logger = Logger.getLogger(JsonFormatDAOImpl.class);
	private SessionFactory sessionFactory;

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> getTwStockMapJSON() {
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		Session session = sessionFactory.openSession();
		String sql = "";
		try {
			
			sql += " SELECT StockId, Name FROM tw_stock_map Order by StockId ASC;";
			
			Query query = session.createSQLQuery(sql.toString());
			list = query.list();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session.isOpen())
				session.close();
		}
		return list;
		
	}
}
