package com.gtbc.general.dao;

import java.util.List;
import java.util.Map;

public interface JsonFormatDAO {
	
	
	public List<Map<String, Object>> getTwStockMapJSON();

}
