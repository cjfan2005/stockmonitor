package com.gtbc.general.action;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.gtbc.general.dao.JsonFormatDAO;
import com.gtbc.interceptor.BaseAction;

public class JsonFormatAction extends BaseAction {
	private static final Logger logger = Logger.getLogger(JsonFormatAction.class);

	private Map stockMapJson;
	
	@Autowired
	private JsonFormatDAO jfDao; //Table: Observation_item
	
	@SuppressWarnings("unchecked")
	public String getTwStockMapJSON() {
		
		List<Map<String, Object>> objList = jfDao.getTwStockMapJSON();
		
		Iterator<?> its = objList.iterator();

		List<Map<String, Object>> smList = new ArrayList<Map<String, Object>>();
		
		Map<String, Object> smItem;
		stockMapJson = new HashMap<String, Object>();
		
		while (its.hasNext()) {
			smItem = new HashMap<String, Object>();
            
			Object[] obj = (Object[]) its.next();
			
			smItem.put("StockId", obj[0].toString());
			smItem.put("Name", obj[1].toString());
			smList.add(smItem);
		}
		
		stockMapJson.put("twStockMapJSONStr", smList);
		return "success";
	}

	public Map getStockMapJson() {
		return stockMapJson;
	}

	public void setStockMapJson(Map stockMapJson) {
		this.stockMapJson = stockMapJson;
	}

	
}
