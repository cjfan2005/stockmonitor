package com.gtbc.general.action;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import org.springframework.beans.factory.annotation.Autowired;

import com.gtbc.general.dao.ObservationListDAO;
import com.gtbc.interceptor.BaseAction;
import com.gtbc.util.PageParameter;



public class ObservationListAction extends BaseAction {
	private static final Logger logger = Logger.getLogger(ObservationListAction.class);
	
	private String itemId; //=StockId or FutureId
	
	// jqgrid
	private List<Map<String, Object>> rows;
	private Map result, shortDataResult, itemChartResult;
	private int total = 0; // 總頁數
	private int records = 0; // 資料總筆數
	private int pageSize = 0, pageSizeSF = 0;
	private int currentPageNo = 0;
	private String sort;
	private String orderBy;
	
	@Autowired
	private ObservationListDAO oiDao; //Table: Observation_item
	
	public String initObservationList() {
		//CHANGE PAGE ONLY.
		return "success";
	}

	@SuppressWarnings("unchecked")
	public String observationList() throws IOException {
		//jqgrid
		PageParameter page = new PageParameter();
		page.setCurrentPageNo(currentPageNo);
		page.setOrderBy(orderBy);
		page.setSort(sort);
		page.setPageSize(pageSize);
		
		String itemNameId = "";
		Map<Integer, String> nameIdMap = new HashMap<Integer, String>();
		Map<Integer, String> itemIdMap = new HashMap<Integer, String>();//itemId = StockId or FutureId.
		List<Map<String, Object>> oiList = oiDao.getObservationItemId();
		
		if(oiList.size()==0 || oiList==null) {
			return null; //no observation data, return empty.
		}
		Map<String, Object> oiItem;
		List<Map<String, Object>> showsList = new ArrayList<Map<String, Object>>();
		for(int i=0; i<oiList.size(); i++) {
//			itemIdMap.put(i, oiList.get(i).get("ItemId").toString());
//			nameIdMap.put(i, oiList.get(i).get("ItemName").toString() + " [" + oiList.get(i).get("ItemId").toString() + "]");
//			itemNameId +="'"+oiList.get(i).get("ItemId").toString()+".tw',";
			oiItem = new HashMap<String, Object>();
			oiItem.put("ItemId", oiList.get(i).get("ItemId").toString());
			oiItem.put("ItemNameId", oiList.get(i).get("ItemName").toString() + " [" + oiList.get(i).get("ItemId").toString() + "]");
			showsList.add(oiItem);
		}
//		itemNameId = itemNameId.substring(0, itemNameId.length()-1);
		
		
		
/*****Mask: because Yahoo Finance API is unstable.
		//JSON PARSER 
		String inputLine=null;
		List<Map<String, Object>> showsList = new ArrayList<Map<String, Object>>();
		Map<String, Object> oiItem;
		String url = "http://query.yahooapis.com/v1/public/yql?format=json&env=store://datatables.org/alltableswithkeys&q=select * from yahoo.finance.quote where symbol in ("+itemNameId+")";
		url = url.replaceAll(" ","%20"); //填滿網址之間空白,避免無法解析。
		
		URL parseUrl = new URL(url);// URL to Parse
		URLConnection yc = parseUrl.openConnection();
		BufferedReader in = null;
		
		try {
			in = new BufferedReader(new InputStreamReader(yc.getInputStream(), "UTF-8"));
		} catch (Exception e1) {
			logger.log(Level.ERROR,  e1.getMessage());
			e1.printStackTrace();
		}
  
		while ((inputLine = in.readLine()) != null) {
			String json_data = inputLine;
			JSONObject jsonObject = new JSONObject(json_data);
			
			try {
				JSONObject resultJsonObj = jsonObject.getJSONObject("query").getJSONObject("results");
				JSONArray quoteJsonArr = resultJsonObj.getJSONArray("quote");
				
				int i=0;
				for (Object o : quoteJsonArr) {
					oiItem = new HashMap<String, Object>();
					JSONObject tutorials = (JSONObject) o;
					
					oiItem.put("ItemId", itemIdMap.get(i).toString());
					oiItem.put("ItemNameId", nameIdMap.get(i).toString());
					
					if(!tutorials.get("Change").toString().equals("null"))
						oiItem.put("Change", (String)tutorials.get("Change"));
					else 
						oiItem.put("Change", "NA");
					
					if(!tutorials.get("DaysLow").toString().equals("null"))
						oiItem.put("DaysLow", (String)tutorials.get("DaysLow"));
					else 
						oiItem.put("DaysLow", "NA");
					
					if(!tutorials.get("DaysHigh").toString().equals("null"))
						oiItem.put("DaysHigh", (String)tutorials.get("DaysHigh"));
					else
						oiItem.put("DaysHigh", "NA");
					
					if(!tutorials.get("DaysRange").toString().equals("null"))
						oiItem.put("DaysRange", (String)tutorials.get("DaysRange"));
					else 
						oiItem.put("DaysRange", "NA");
					
					if(!tutorials.get("Volume").toString().equals("null"))
						oiItem.put("Volume", (String)tutorials.get("Volume"));
					else
						oiItem.put("Volume", "NA");
					
					showsList.add(oiItem);
					i++;
				}
				
			} catch (JSONException e) {
				logger.log(Level.ERROR,  e.getMessage());
				e.printStackTrace();
			}
		}
		in.close();
******/		
		int count = showsList.size();
		showsList = dataPagination(showsList, page);
		rows = showsList;
		int total = count % pageSize == 0 ? (count / pageSize) : (count / pageSize + 1);

		result = new HashMap<String, Object>();
		result.put("pageSize", pageSize);
		result.put("total", total);
		result.put("count", count);
		result.put("currentPageNo", currentPageNo);
		result.put("rows", rows);
		
		return "success";
	}
	
	@SuppressWarnings({ "unchecked" })
	public String getItemIdShortData() {
		if ((itemId == null || itemId.isEmpty())) {
			return null;
		}
		
		List<Map<String, Object>> objList = oiDao.getItemIdShortData(itemId);
		
		Iterator<?> shortItems = objList.iterator();

		List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
		Map<String, Object> shortDataMap;

		//arrange data
		while (shortItems.hasNext()) {
			shortDataMap = new HashMap<String, Object>();

			Object[] obj = (Object[]) shortItems.next();

			shortDataMap.put("TradingDate", obj[0]);
			shortDataMap.put("Open", obj[1]);
			shortDataMap.put("High", obj[2]);
			shortDataMap.put("Low", obj[3]);
			shortDataMap.put("Price", obj[4]);
			shortDataMap.put("Volume", obj[5]);
			resultList.add(shortDataMap);
		}

		rows = resultList;
		shortDataResult = new HashMap<String, Object>();
		shortDataResult.put("rows", rows);
		return SUCCESS;
	}
	
	public String initItemCharts() {
		return "success";
	}
	
	//combine 2 charts in one page.(Historical & Oscillation.)
	@SuppressWarnings("unchecked")
	public String itemCharts() {
		if ((itemId == null || itemId.isEmpty())) {
			return null;
		}
		PageParameter page = new PageParameter();
		page.setCurrentPageNo(currentPageNo);
		page.setOrderBy(orderBy);
		page.setSort(sort);
		page.setPageSize(pageSize);
		
		
		//Historical & Oscillation.
		List<Map<String, Object>> objList = oiDao.getItemDataForCharts(itemId);
		
		int count = objList.size();
		
		objList = dataPagination(objList, page);

		rows = objList;

		int total = count % pageSize == 0 ? (count / pageSize) : (count / pageSize + 1);

		itemChartResult = new HashMap<String, Object>();
		itemChartResult.put("pageSize", pageSize);
		itemChartResult.put("total", total);
		itemChartResult.put("count", count);
		itemChartResult.put("currentPageNo", currentPageNo);
		itemChartResult.put("rows", rows);
		
		return "success";
	}
	
	//getter and setter.
	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public List<Map<String, Object>> getRows() {
		return rows;
	}

	public void setRows(List<Map<String, Object>> rows) {
		this.rows = rows;
	}

	public Map getResult() {
		return result;
	}

	public void setResult(Map result) {
		this.result = result;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public int getRecords() {
		return records;
	}

	public void setRecords(int records) {
		this.records = records;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getPageSizeSF() {
		return pageSizeSF;
	}

	public void setPageSizeSF(int pageSizeSF) {
		this.pageSizeSF = pageSizeSF;
	}

	public int getCurrentPageNo() {
		return currentPageNo;
	}

	public void setCurrentPageNo(int currentPageNo) {
		this.currentPageNo = currentPageNo;
	}

	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

	public String getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}

	public Map getShortDataResult() {
		return shortDataResult;
	}

	public void setShortDataResult(Map shortDataResult) {
		this.shortDataResult = shortDataResult;
	}

	public Map getItemChartResult() {
		return itemChartResult;
	}

	public void setItemChartResult(Map itemChartResult) {
		this.itemChartResult = itemChartResult;
	}
		
}
