
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/jqGrid/css/ui.jqgrid.css" />
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/jqGrid/themes/redmond/jquery-ui-custom.css" />

<script type="text/javascript" src="${pageContext.request.contextPath}/jqGrid/js/jquery.jqGrid.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/jqGrid/js/i18n/grid.locale-en.js"></script>

<script type="text/javascript">
	$.jgrid.no_legacy_api = true;
	$.jgrid.useJSON = true;
</script>

