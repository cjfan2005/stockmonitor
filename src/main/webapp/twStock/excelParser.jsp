<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.gtbc.stock.bean.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<%@ include file="/IncludePage.jsp"%>
<jsp:include page="/jqGridPage.jsp" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>GTBC-</title>

<script type="text/javascript">
    
</script>

</head>
<body>
<div id="grailsLogo"><a href="#"><img src="${pageContext.request.contextPath}/images/gtbc_logo.png" alt="GTBC" border="0" /></a></div>
<jsp:include page="/menus_general.jsp"></jsp:include>
<br><br>
<div class="path">
PATH： TW.Stock :
Excel Parser Tool</div>
<div class="body">
<div class="body">

<div class="dialog">
<s:actionerror/><s:actionmessage/>

<form action="" method="post" name="form1" id="form1">

<table width="650" >
  <tr>
    <th>Stock Id：</th>
    <td>
      <s:textfield name="stockId" id="stockId" type="text" value="%{stockId}" theme="simple"></s:textfield>
    </td>
    <th>Name：</th>
    <td>
      <s:textfield name="name" id="name" type="text" value="%{name}" theme="simple"></s:textfield>
    </td>
    <td>
      <s:submit onclick="searchStock()" cssClass="comfirm" value="Query" theme="simple"/>
      <input type="reset" value="Reset"  class="buttonBlue">
    </td>
  </tr>
</table>
  
  
</form>
</div>
</div>
</div>
</body>
</html>
 