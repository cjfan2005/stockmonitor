<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.gtbc.stock.bean.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- Datepicker calendar start -->
<script src="${pageContext.request.contextPath}/js/datepicker/WdatePicker.js"></script>
<script charset="utf-8" src="${pageContext.request.contextPath}/js/datepicker/lang/zh-tw.js"></script>
<link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/js/datepicker/skin/WdatePicker.css" />
<!-- Datepicker calendar end -->
<head>
<%@ include file="/IncludePage.jsp"%>
<jsp:include page="/jqGridPage.jsp" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>GTBC-</title>

<script type="text/javascript">
	function clickme(stockId) {		
		var url = '${pageContext.request.contextPath}/twStock/InitGenChartFIAction.do?stockId='+stockId;
		popup(url);
//		$('#form1').attr('action', '${pageContext.request.contextPath}/twStock/InitGenChartFIAction.do?stockId='+stockId);	
//		$('#form1').submit();
	}
	
	function popup(url) {
		params  = 'width='+screen.width;
		params += ', height='+screen.height;
		params += ', top=0, left=0'
		params += ', fullscreen=0';
		
		newwin=window.open(url,'popUpWindow', params);
		if (window.focus) {newwin.focus()}
		return false;
	}
	
	$(document).ready(function() {
		$("#grid").jqGrid({
			url:encodeURI('${pageContext.request.contextPath}/twStock/ExecuteForceIndexAction.do?stockId=${stockId}&name=${name}'),
			//url:'${pageContext.request.contextPath}/user/ListUserConditionAction.do',
			datatype: "JSON",
			height: "100%+50", 
			width:"950",
			//idPrefix: "staff_",
			colNames:['Name','SId','Date', 'VOL', 'OPEN', 'HIGH','LOW', 'Close','6D-FI','14D-FI','20D-FI', ''],
	        colModel:[
                      {name:'Name', index:'Name', width:25, align:'center',hidden: true},
                      {name:'StockId', index:'StockId', width:25, align:'center',hidden: true},
	                  {name:'TradingDate', index:'TradingDate', width:25, align:'center'}, 
	                  {name:'Volume', index:'Volume', width:20, align:'right',hidden: false},
	                  {name:'Open', index:'Open', width:20, align:'right', hidden: false},
	                  {name:'High', index:'High', width:20, align:'right'},
	                  {name:'Low', index:'Low', width:20, align:'right'},
	                  {name:'Close', index:'Close', width:20, align:'right'},
	                  
	                  {name:'ForceIndex6', index:'ForceIndex20', width:20, align:'right', hidden: false},
	                  {name:'ForceIndex14', index:'ForceIndex20', width:20, align:'right', hidden: false},
	                  {name:'ForceIndex20', index:'ForceIndex20', width:20, align:'right', hidden: false},
	                  {   //Show Button
	                	  name:'action',index:'action',sortable:false, width:20, align:'left',
	                	  formatter:function (cellvalue, options, rowObject) {
	                	        return '<input class="buttonBlue" type="button" onClick="clickme(\'' + rowObject.StockId + '\')" type="button" value="買賣力道"/>'
	                	  }
	                  },
	                  
	        ],
	        sortorder: 'ASC',
	        sortname:'TradingDate',
	        viewrecords: true,
	        gridview: true, 
	        rownumbers: false,
	        rowNum: 365, 
	        rowList: [365,730], 
	        pgbuttons: true,
	        pginput: true,
	        pager: "#gridPager",
	        prmNames:{rows:"pageSize",page:"currentPageNo",sort:"sort",order:"orderBy",total:"totalPages",records:"records",search:"search"}, 
	        //caption: "歷史交易明細",
	        loadComplete: function(data){
	        	//document.getElementById("ulList").style.zIndex = 1000;//避免menu圖層蓋住網頁內容,menu顯示在最上層.
	        	$("#ulList").css("zIndex", 1000);//避免menu圖層蓋住網頁內容,menu顯示在最上層.
	        	
	        	$("#mainFunction").hide();//隱藏jqgrid表單
	        	//arrage data for HighChart.
	        	var stockNameId, td=[], close=[], vol=[];
	        	var FI20D=[], FI14D=[], FI6D=[];
	        	    
	        	for (var i=0 ; i<data.rows.length; i++)
	        	{
	        		if(i==0)
	        		  stockNameId = data.rows[0].Name + " [" + data.rows[0].StockId + "]"
	        			
	        	    td[i] = data.rows[i].TradingDate;
	        	    close[i] = data.rows[i].Close;
	        	    vol[i] = data.rows[i].Volume;
	        	    FI20D[i] = data.rows[i].ForceIndex20;
	        	    FI14D[i] = data.rows[i].ForceIndex14;
	        	    FI6D[i] = data.rows[i].ForceIndex6;
	        	}
	        	
	        	//forward to highChart!
	        	drawHighChart(stockNameId, td, close, vol, FI20D, FI14D, FI6D);
	        }
	    });
	});

//highCharts
function drawHighChart(stockNameId, td, close, vol, FI20D, FI14D, FI6D) {
    $('#container').highcharts({
        chart: {
            zoomType: 'xy'
        },
        title: {
            text: '買賣力道指數 (BestBuyRange: 0.6<FI<1.4 )'
        },
        credits: {
            enabled: false //移除highchats右下方logo.
        },
        subtitle: {
            text: stockNameId + ' © GTBC Inc.'
        },
        xAxis: [{
            categories: td, //TradingDate
            crosshair: true
        }],
        yAxis: [{ // Primary yAxis
            
        	plotLines: [{
                value: 0.6,
                color: 'yellow',
                dashStyle: 'shortdash',
                width: 2,
                label: {
                    text: 'minimum=0.6'
                }
            }, {
                value: 1.4,
                color: 'yellow',
                dashStyle: 'shortdash',
                width: 2,
                label: {
                    text: 'maximum=1.4'
                }
            }],
        	labels: {
                format: '{value}',
                style: {
                    color: 'limegreen'
                }
            },
            title: {
                text: '買賣力道指數  (Force Index, FI)',
                style: {
                    color: 'limegreen'
                }
            },
            opposite: true

        }, { // Secondary yAxis
            gridLineWidth: 0,
            title: {
                text: '成交量 (Volume)',
                style: {
                    //color: Highcharts.getOptions().colors[0]
                	color: 'skyblue '
                }
            },
            labels: {
                format: '{value}',
                style: {
                    color: 'skyblue'
                    //color: Highcharts.getOptions().colors[0]
                }
            }

        }, { // Tertiary yAxis
            gridLineWidth: 0,
            title: {
                text: '收盤價 (Close)',
                style: {
                    color: 'red'
                }
            },
            labels: {
                format: '{value}',
                style: {
                    color: 'red'
                }
            },
            opposite: true
        }],
        tooltip: {
            shared: true
        },
        legend: {
            layout: 'vertical',
            align: 'left',
            x: 0,
            verticalAlign: 'top',
            y: 0,
            floating: true,
            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
        },
        series: [{
            name: 'Volume',
            type: 'column',
            yAxis: 1,
            data: vol,
            color: 'skyblue',
            tooltip: {
                valueSuffix: '張'
            }

        }, {
            name: 'Close',
            type: 'spline',
            yAxis: 2,
            data: close,
            color: 'red',
            marker: {
                enabled: false
            },
            dashStyle: 'shortdot',
            tooltip: {
                valueSuffix: '元'
            }

        }, {
            name: '20-day FI',
            type: 'spline',
            data: FI20D,
            color: 'limegreen',
            tooltip: {
                valueSuffix: ''
            }
        }, {
            name: '14-day FI',
            type: 'spline',
            data: FI14D,
            color: 'lightslategray',
            tooltip: {
                valueSuffix: ''
            }
        }, {
            name: '6-day FI',
            type: 'spline',
            data: FI6D,
            color: 'orange',
            tooltip: {
                valueSuffix: ''
            }
        }]
    });
}
</script>

</head>
<body>
<div id="grailsLogo"><a href="#"><img src="${pageContext.request.contextPath}/images/gtbc_logo.png" alt="GTBC" border="0" /></a></div>
<jsp:include page="/menus_general.jsp"></jsp:include>
<br><br>
<div class="path">
PATH： TW.Stock :
Force Index Analysis</div>
<div id="tabs5">
  <ul>
    <li id="current">
       <a id="a1" href="${pageContext.request.contextPath}/twStock/ForceIndexAction.do">
         <span id="s1">買賣力道指數</span>
       </a>
    </li>
    <li>
       <a id="a2" href="${pageContext.request.contextPath}/twStock/bestBuyStock.jsp">
         <span id="s2">Best Buy Stocks</span>
       </a>
    </li>
  </ul>
</div>
<div class="body">
<div class="body">

<div class="dialog">
<s:actionerror/><s:actionmessage/>


<form action="" method="post" name="form1" id="form1">

<table width="650" >
  <tr>
    <th>StockId：</th>
    <td>
      <s:textfield name="stockId" id="stockId" type="text" value="%{stockId}" theme="simple"></s:textfield>
    </td>
    <th>Name：</th>
    <td>
      <s:textfield name="name" id="name" type="text" value="%{name}" theme="simple"></s:textfield>
    </td>
    <td>
      <s:submit onclick="searchStock()" cssClass="comfirm" value="Query" theme="simple"/>
    </td>
  </tr>
</table>

  <!-- highcharts -->
  <script src="https://code.highcharts.com/highcharts.js"></script>
  <script src="https://code.highcharts.com/modules/exporting.js"></script>
  <div id="container" style="min-width: 310px; height: 600px; margin: 0 auto"></div>
  
  <div id="all">
  <div id="mainFunction">
	<left>
		<table id="grid"></table>
		<div id="gridPager"></div>
	</left>
	</div>
  </div>
</form>
</div>
</div>
</div>
</body>
</html>
 