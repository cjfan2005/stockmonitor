<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.gtbc.stock.bean.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- Datepicker calendar start -->
<script src="${pageContext.request.contextPath}/js/datepicker/WdatePicker.js"></script>
<script charset="utf-8" src="${pageContext.request.contextPath}/js/datepicker/lang/zh-tw.js"></script>
<link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/js/datepicker/skin/WdatePicker.css" />
<!-- Datepicker calendar end -->
<head>
<%@ include file="/IncludePage.jsp"%>
<jsp:include page="/jqGridPage.jsp" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>GTBC-</title>

<script type="text/javascript">
	function clickme(stockId) {		
		var url = '${pageContext.request.contextPath}/twStock/InitGenChartFIAction.do?stockId='+stockId;
		popup(url);
//		$('#form1').attr('action', '${pageContext.request.contextPath}/twStock/InitGenChartFIAction.do?stockId='+stockId);	
//		$('#form1').submit();
	}
	
	function popup(url) {
		params  = 'width='+screen.width;
		params += ', height='+screen.height;
		params += ', top=0, left=0'
		params += ', fullscreen=0';
		
		newwin=window.open(url,'popUpWindow', params);
		if (window.focus) {newwin.focus()}
		return false;
	}
	
	$(document).ready(function() {
		$("#bbGrid").jqGrid({
			url:encodeURI('${pageContext.request.contextPath}/twStock/BestBuyStockAction.do'),
			datatype: "JSON",
			height: "100%+50", 
			width:"850",
			//idPrefix: "staff_",
			colNames:['Id','SId','力道指數','LINE','結算日'],
	        colModel:[
                      {name:'Name&Id', index:'Name&Id', width:18, align:'right',hidden: false, sort:false},
                      {name:'StockId', index:'StockId', width:25, align:'center',hidden: true},
	                  
	                  {name:'ForceIndexVal', index:'ForceIndexVal', width:30, align:'right', hidden: false},
	                  {   //Show Button
	                	  name:'action',index:'action',sortable:false, width:10, align:'left',
	                	  formatter:function (cellvalue, options, rowObject) {
	                	        return '<input class="buttonBlue" type="button" onClick="clickme(\'' + rowObject.StockId + '\')" type="button" value="線圖"/>'
	                	  }
	                  },
	                  {name:'AccountingDate', index:'AccountingDate', width:16, align:'center',hidden: false}, 
	                  
	        ],
	        sortorder: 'DESC',
	        sortname:'ForceIndexVal',
	        viewrecords: true,
	        gridview: true, 
	        rownumbers: true,
	        rowNum: 20, 
	        rowList: [20,40], 
	        pgbuttons: true,
	        pginput: true,
	        pager: "#gridPager",
	        prmNames:{rows:"pageSize",page:"currentPageNo",sort:"sort",order:"orderBy",total:"totalPages",records:"records",search:"search"}, 
	        caption: "Best Buy Stock List",
	        subGrid : true,
	        
		        
	    	//subGrid start >>>
	    	subGridRowExpanded: function(subgrid_id, row_id) {
				var parentData = jQuery("#bbGrid").jqGrid('getRowData',row_id);
				//alert(parentData.StockId);
				var subgrid_table_id;
				subgrid_table_id = subgrid_id+"_t";
			 
				$("#"+subgrid_id).html("<table id='"+ subgrid_table_id+"' class='scroll'></table>");
			  	$("#"+subgrid_table_id).jqGrid({
			  		url:"${pageContext.request.contextPath}/twStock/GetBestBuyShortDataAction.do?stockId="+parentData.StockId,
			  		datatype: "json",
			  		width:"740",
			  		colNames: ['TDate','Open','High','Low','Close','Vol','6D-FI','14D-FI','20D-FI'],
			  		colModel: [
			      		{name:"TradingDate",index:"TradingDate",width:18, align:'center',sortable:false},
			      		{name:"Open",index:"Open",width:20, align:'right', sortable:false},
			      		{name:"High",index:"High",width:20, align:'right', sortable:false},
			      		{name:"Low",index:"Low",width:20, align:'right', sortable:false},
			      		{name:"Price",index:"Price",width:20, align:'right', sortable:false},
			      		{name:"Volume",index:"Volume",width:20, align:'right', sortable:false},
			      		{name:"ForceIndex6",index:"ForceIndex6",width:25, sortable:false},
			      		{name:"ForceIndex14",index:"ForceIndex14",width:25, sortable:false},
			      		{name:"ForceIndex20",index:"ForceIndex20",width:25, sortable:false}
			  		],
			  		height: "100%"+20,
			  		rowNum:10,
			  		prmNames:{rows:"pageSize"},
			  	});
			},
	    	//subgrid end <<<
        
	    });
	});
</script>

</head>
<body>
<div id="grailsLogo"><a href="#"><img src="${pageContext.request.contextPath}/images/gtbc_logo.png" alt="GTBC" border="0" /></a></div>
<jsp:include page="/menus_general.jsp"></jsp:include>
<br><br>
<div class="path">
PATH： TW.Stock :
Force Index Analysis</div>
<div id="tabs5">
  <ul>
    <li id="current">
       <a id="a1" href="${pageContext.request.contextPath}/twStock/ForceIndexAction.do">
         <span id="s1">FI Historical</span>
       </a>
    </li>
    <li>
       <a id="a2" href="${pageContext.request.contextPath}/twStock/bestBuyStock.jsp">
         <span id="s2">Best Buy Stocks</span>
       </a>
    </li>
  </ul>
</div>
<div class="body">
<div class="body">

<div class="dialog">
<s:actionerror/><s:actionmessage/>

<form action="" method="post" name="form1" id="form1">
  <div id="all">
  <div id="mainFunction">
	<left>
		<table id="bbGrid"></table>
		<div id="gridPager"></div>
	</left>
	</div>
  </div>
</form>
</div>
</div>
</div>
</body>
</html>
 