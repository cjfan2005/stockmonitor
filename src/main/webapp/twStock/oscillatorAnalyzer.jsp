<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.gtbc.stock.bean.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>

<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/jqGrid/themes/redmond/jquery-ui-custom.css" />

<%@ include file="/IncludePage.jsp"%>
<jsp:include page="/jqGridPage.jsp" />

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>GTBC-Oscillator Analyzer</title>
<script type="text/javascript">

	$(document).ready(function() {
		$("#grid").jqGrid({
			url:encodeURI('${pageContext.request.contextPath}/twStock/OscillatorReultAction.do?stockId=${stockId}&fastEMAParam=${fastEMAParam}&slowEMAParam=${slowEMAParam}&signalParam=${signalParam}'),
			datatype: "JSON",
			height: "100%+80", 
			width:"960",
			
			colNames:['SName','SId','Date', 'Close', 'FastEMA', 'SlowEMA','MACD', 'Signal','HistogramVal', 'ConHistoVal',
			          'MA5','MA10','MA20'],
	        colModel:[
                 {name:'StockName', index:'StockNmae', width:25, align:'center',hidden: true},
                 {name:'StockId', index:'StockId', width:25, align:'center',hidden: true},
                 {name:'TradingDate', index:'TradingDate', width:25, align:'center'}, 
                 {name:'Close', index:'Close', width:25, align:'right',hidden: false},
                 {name:'FastEMA', index:'FastEMA', width:25, align:'right'},
                 {name:'SlowEMA', index:'SlowEMA', width:25, align:'right', hidden: false},
                 {name:'MACD', index:'MACD', width:25, align:'right'},
                 {name:'Signal', index:'Signal', width:30, align:'right'},
                 {name:'HistogramVal', index:'HistogramVal',  width:30, align:'right'},
                 {name:'ConHistoVal', index:'ConHistoVal',  width:5, align:'center'},
                 {name:'MA5', index:'MA5',  width:1, align:'center'},
                 {name:'MA10', index:'MA5',  width:1, align:'center'},
                 {name:'MA20', index:'MA5',  width:1, align:'center'},
	        ],
	        sortorder: 'DESC',
	        sortname:'TradingDate',
	        viewrecords: true,
	        gridview: true, 
	        rownumbers: false,
	        rowNum: 1000, 
	        rowList: [1000], 
	        pgbuttons: true,
	        pginput: true,
	        pager: "#gridPager",
	        prmNames:{rows:"pageSize",page:"currentPageNo",sort:"sort",order:"orderBy",total:"totalPages",records:"records",search:"search"}, 
	        //caption: "Oscillator Algorithm Results",
	        loadComplete: function(data) {
	        	
	        	$("#mainFunction").hide();//隱藏jqgrid表單
	        	//arrage data for HighChart.
	        	var stockNameId, td=[], c=[], hv=[], ma5=[], ma10=[], ma20=[];
	        	    
	        	for (var i=0 ; i<data.rows.length; i++)
	        	{
	        		if(i==0)
	        		  stockNameId = data.rows[0].StockName + " [" + data.rows[0].StockId + "]"
	        			
	        	    td[i] = data.rows[i].TradingDate;
	        	    c[i] = data.rows[i].Close;
	        	    hv[i] = data.rows[i].HistogramVal;
	        	    ma5[i] = data.rows[i].MA5;
	        	    ma10[i] = data.rows[i].MA10;
	        	    ma20[i] = data.rows[i].MA20;
	        	}
	        	drawHighChart(stockNameId, td, c, hv, ma5, ma10, ma20);//forward to highChart!
	        }
	    });
		
	});	

	//high chart
function drawHighChart(stockNameId, td, c, hv, ma5, ma10, ma20) {
	
	$('#container').highcharts({
		
		
        chart: {
            zoomType: 'xy'
        },
        credits: {
            enabled: false //移除highchats右下方logo.
        },
        title: {
            text: 'Oscillation Analysis Report'
        },
        subtitle: {
            text:  stockNameId + ' © GTBC Inc.' //show StockName and StockId
        },
        xAxis: [{
            categories: td, //TradingDate
            crosshair: true
        }],
        yAxis: [{ // Primary yAxis
            labels: {
                format: '${value}',
                style: {
                    color: "red"
                }
            },
            title: {
                text: 'Close Price',
                style: {
                    color: "red"
                }
            }
        }, { // Secondary yAxis
            title: {
                text: 'Histogram Values',
                style: {
                    color: Highcharts.getOptions().colors[0]
                }
            },
            labels: {
                format: '{value}',
                style: {
                    color: Highcharts.getOptions().colors[0]
                }
            },
            opposite: true
        }],
        tooltip: {
            shared: true
        },
        
        legend: {
            layout: 'vertical',
            align: 'left',
            x: 0,
            verticalAlign: 'top',
            y: 0,
            floating: true,
            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
        },
        
        series: [{
            name: 'Histogram Value',
            type: 'column',
            yAxis: 1,
            data: hv, //Histogram Value
            tooltip: {
                valueSuffix: ''
            }

        }, {
            name: 'Close Price',
            type: 'spline',
            color: "red",
            yAxis: 0,
            data: c,//close
            tooltip: {
                valueSuffix: ''
            }
        }, {
            name: 'MA5',
            type: 'spline',
            yAxis: 0,
            dashStyle: 'shortdot',
            data: ma5,
            color: 'limegreen',
        }, {
            name: 'MA10',
            type: 'spline',
            yAxis: 0,
            dashStyle: 'shortdot',
            data: ma10,
            color: 'orange',
        }, {
            name: 'MA20',
            type: 'spline',
            yAxis: 0,
            dashStyle: 'shortdot',
            data: ma20,
            color: 'lightslategray',
        }]
    });	
    
}

	
</script>

</head>
<body>
<div id="grailsLogo"><a href="#"><img src="${pageContext.request.contextPath}/images/gtbc_logo.png" alt="GTBC" border="0" /></a></div>
<jsp:include page="/menus_general.jsp"></jsp:include>
<br><br>
<div class="path">
PATH： TW.Stock :
Oscillator Analyzer</div>
<div class="body">
<div class="body">

<div class="dialog">
<s:actionerror/><s:actionmessage/>


<form action="" method="post" name="form1" id="form1">

<table width="650" >
  <tr>
    <th>StockId：</th>
    <td>
      <s:textfield name="stockId" id="stockId" type="text" value="%{stockId}" cssClass="required" theme="simple"></s:textfield>
    </td>
    <th>Signal：</th>
    <td>
      <s:textfield name="signalParam" id="signalParam" type="text" value="9" cssClass="required number" theme="simple"></s:textfield>
    </td>
  </tr>
  <tr>
    <th>FastEMA：</th>
    <td>
      <s:textfield name="fastEMAParam" id="fastEMAParam" type="text" value="12" cssClass="required number" theme="simple"></s:textfield>
    </td>
    
    <th>SlowEMA：</th>
    <td>
      <s:textfield name="slowEMAParam" id="slowEMAParam" type="text" value="26" cssClass="required number" theme="simple"></s:textfield>
    </td>
    
    <td>
      <s:submit onclick="getOscillatorResult()" cssClass="comfirm" value="Query" theme="simple"/>
      
    </td>
  </tr>
</table>
  <!-- high chart -->
  <script src="https://code.highcharts.com/highcharts.js"></script>
  <script src="https://code.highcharts.com/modules/exporting.js"></script>
  <div id="container" style="min-width: 310px; height: 600px; margin: 0 auto"></div><br>
  
  <!-- jqgrid -->
  <div id="all">
  <div id="mainFunction">
	<left>
		<table id="grid"></table>
		<div id="gridPager"></div>
	</left>
	</div>
  </div>
<!--   <table id="testTable">
	 <td>
	   選擇使用者帳號(User ID) : <span id="showUserId"></span>
	 </td>
  </table> -->
</form>
</div>
</div>
</div>
</body>
</html>
 