<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.gtbc.stock.bean.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- Datepicker calendar start -->
<script src="${pageContext.request.contextPath}/js/datepicker/WdatePicker.js"></script>
<script charset="utf-8" src="${pageContext.request.contextPath}/js/datepicker/lang/zh-tw.js"></script>
<link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/js/datepicker/skin/WdatePicker.css" />
<!-- Datepicker calendar end -->
<head>
<%@ include file="/IncludePage.jsp"%>
<jsp:include page="/jqGridPage.jsp" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>GTBC-Stock Historical</title>

<script type="text/javascript">
    
	function clickme() {
		var stockId = document.getElementById("stockId").innerHTML;
		alert(stockId);
		var url = '${pageContext.request.contextPath}/twStock/InitGenChartAction.do?stockId='+stockId;
		popup(url);
		//$('#form1').attr('action','${pageContext.request.contextPath}/twStock/InitGenChartAction.do?stockId='+stockId);
		//$('#form1').submit();
	}
	function popup(url)
	{
		params  = 'width='+screen.width;
		params += ', height='+screen.height;
		params += ', top=0, left=0'
		params += ', fullscreen=0';
		
		newwin=window.open(url,'popUpWindow', params);
		if (window.focus) {newwin.focus()}
		return false;
	}
	
	$(document).ready(function() {
		$("#grid").jqGrid({
			url:encodeURI('${pageContext.request.contextPath}/twStock/ListStockConditionAction.do?stockId=${stockId}&name=${name}&tradingDateStr=${tradingDateStr}&tradingDateEnd=${tradingDateEnd}'),
			datatype: "JSON",
			height: "100%+50", 
			width:"800",
			colNames:['Name','SId','Date', 'VOL', 'OPEN', 'HIGH','LOW', 'Close',''],
	        colModel:[
                  {name:'Name', index:'Name', width:25, align:'center',hidden: true},
                  {name:'StockId', index:'StockId', width:25, align:'center',hidden: true},
                  {name:'TradingDate', index:'TradingDate', width:25, align:'center'}, 
                  {name:'Volume', index:'Volume', width:25, align:'right',hidden: false},
                  {name:'Open', index:'Open', width:25, align:'right', hidden: false},
                  {name:'High', index:'High', width:25, align:'right'},
                  {name:'Low', index:'Low', width:25, align:'right'},
                  {name:'Price', index:'Price', width:25, align:'right'},
                  {   //Show Button
                	  name:'action',index:'action',sortable:false, width:20, align:'left',
                	  formatter:function (cellvalue, options, rowObject) {    
                		  //alert("flowID:" + rowObject.flowID); //get row data
                	      return '<input class="buttonBlue" type="button" onClick="clickme(\' ' + rowObject.StockId + '\')" type="button" value="圖"/>'
                	  }
                  },
	        ],
	        sortorder: 'ASC',
	        sortname:'TradingDate',
	        viewrecords: true,
	        gridview: true, 
	        rownumbers: false,
	        rowNum: 1200, 
	        rowList: [1000,2000], 
	        pgbuttons: true,
	        pginput: true,
	        pager: "#gridPager",
	        prmNames:{rows:"pageSize",page:"currentPageNo",sort:"sort",order:"orderBy",total:"totalPages",records:"records",search:"search"}, 
	        //caption: "歷史交易明細",
	        loadComplete: function(data) {
	        	$("#mainFunction").hide();//隱藏jqgrid表單
	        	
	        	var stockName, stockId, i,stockIdName, dummyArr=[];
	        	var stockDataArray=[];
	        	
	        	for (i=0 ; i<data.rows.length; i++)
	        	{	
	        		if(i==0){
	        			stockId = data.rows[0].StockId;
	        			stockName = data.rows[0].Name;
	        		}
	        		   
	        		stockDataArray[i] = new Array(6);
	        		stockDataArray[i][0] = Number((data.rows[i].TradingDate+28800)+'000');	 
	        		stockDataArray[i][1] = data.rows[i].Open;
	        		stockDataArray[i][2] = data.rows[i].High;
	        		stockDataArray[i][3] = data.rows[i].Low;
	        		stockDataArray[i][4] = data.rows[i].Price;
	        		stockDataArray[i][5] = data.rows[i].Volume;
	        	}
	        	
	        	drawHighCharts(stockDataArray, i, stockId, stockName);
	        }
	    });
	});	
	
	
//highCharts
function drawHighCharts(stockDataArray, counter, stockId, stockName) {

	//theme start by kent >>>>>>>>>>>>>
	/**
	 * Sand-Signika theme for Highcharts JS
	 * @author Torstein Honsi
	 */

	// Load the fonts
	Highcharts.createElement('link', {
	   href: 'https://fonts.googleapis.com/css?family=Signika:400,700',
	   rel: 'stylesheet',
	   type: 'text/css'
	}, null, document.getElementsByTagName('head')[0]);

	// Add the background image to the container
	Highcharts.wrap(Highcharts.Chart.prototype, 'getContainer', function (proceed) {
	   proceed.call(this);
	   this.container.style.background = 'url(http://www.highcharts.com/samples/graphics/sand.png)';
	});


	Highcharts.theme = {
	   colors: ["#f45b5b", "#8085e9", "#8d4654", "#7798BF", "#aaeeee", "#ff0066", "#eeaaee",
	      "#55BF3B", "#DF5353", "#7798BF", "#aaeeee"],
	   chart: {
	      backgroundColor: null,
	      style: {
	         fontFamily: "Signika, serif"
	      }
	   },
	   title: {
	      style: {
	         color: 'black',
	         fontSize: '16px',
	         fontWeight: 'bold'
	      }
	   },
	   subtitle: {
	      style: {
	         color: 'black'
	      }
	   },
	   tooltip: {
	      borderWidth: 0
	   },
	   legend: {
	      itemStyle: {
	         fontWeight: 'bold',
	         fontSize: '13px'
	      }
	   },
	   xAxis: {
	      labels: {
	         style: {
	            color: '#6e6e70'
	         }
	      }
	   },
	   yAxis: {
	      labels: {
	         style: {
	            color: '#6e6e70'
	         }
	      }
	   },
	   plotOptions: {
	      series: {
	         shadow: true
	      },
	      candlestick: {
	         lineColor: '#404048'
	      },
	      map: {
	         shadow: false
	      }
	   },

	   // Highstock specific
	   navigator: {
	      xAxis: {
	         gridLineColor: '#D0D0D8'
	      }
	   },
	   rangeSelector: {
	      buttonTheme: {
	         fill: 'white',
	         stroke: '#C0C0C8',
	         'stroke-width': 1,
	         states: {
	            select: {
	               fill: '#D0D0D8'
	            }
	         }
	      }
	   },
	   scrollbar: {
	      trackBorderColor: '#C0C0C8'
	   },

	   // General
	   background2: '#E0E0E8'

	};
	// Apply the theme
	Highcharts.setOptions(Highcharts.theme);
	//high chart theme END by kent <<<<<<<<<<<<<<
	
   // split the data set into ohlc and volume
   var ohlc = [],
       volume = [],
       dataLength = counter;//data.length,
       
       // set the allowed units for data grouping
       groupingUnits = [[
           'week',                         // unit name
           [1]                             // allowed multiples
       ], [
           'month',
           [1, 2, 3, 4, 6]
       ]],

       i = 0;

   for (i; i < dataLength; i += 1) {
       ohlc.push([
           stockDataArray[i][0], // the date
           stockDataArray[i][1], // open
           stockDataArray[i][2], // high
           stockDataArray[i][3], // low
           stockDataArray[i][4]  // close
       ]);

       volume.push([
           stockDataArray[i][0], // the date
           stockDataArray[i][5]  // the volume
       ]);
   }
   
   // create the chart
   $('#container').highcharts('StockChart', {

       rangeSelector: {
           selected: 1
       },
		
       credits: {
           enabled: false //移除highchats右下方logo.
       },
       
       title: {
           text: stockName + ' [' + stockId + '] Historical Price © GTBC Inc. Kent.FAN'
       },

       yAxis: [{
           labels: {
               align: 'right',
               x: -3
           },
           title: {
               text: '開高低收'//OHLC
           },
           height: '60%',
           lineWidth: 2
       }, {
           labels: {
               align: 'right',
               x: -3
           },
           title: {
               text: '成交量'//VOL
           },
           top: '65%',
           height: '35%',
           offset: 0,
           lineWidth: 2
       }],
       
       series: [{
           type: 'candlestick',
           name: stockName, 
           data: ohlc,
           dataGrouping: {
               units: groupingUnits
           }
       }, {
           type: 'column',
           name: 'Vol',
           data: volume,
           yAxis: 1,
           dataGrouping: {
               units: groupingUnits
           }
       }]
   });
    
}
		
</script>

</head>
<body>
<div id="grailsLogo"><a href="#"><img src="${pageContext.request.contextPath}/images/gtbc_logo.png" alt="GTBC" border="0" /></a></div>
<jsp:include page="/menus_general.jsp"></jsp:include>
<br><br>
<div class="path">
PATH： TW.Stock :
Historical Prices</div>
<div class="body">
<div class="body">

<div class="dialog">
<s:actionerror/><s:actionmessage/>

<form action="" method="post" name="form1" id="form1">

<table width="650" >
  <tr>
    <th>Stock Id：</th>
    <td>
      <s:textfield name="stockId" id="stockId" type="text" value="%{stockId}" theme="simple" hint="2330"></s:textfield>
    </td>
    <th>Stock Name：</th>
    <td>
      <s:textfield name="name" id="name" type="text" value="%{name}" theme="simple"></s:textfield>
    </td>
    <td>
      <s:submit onclick="searchStock()" cssClass="comfirm" value="Query" theme="simple"/>
    </td>
  </tr>
  <!--
  <tr>
    <th>Time Period：</th>
    <td>
     <s:textfield type="text" id="tradingDateStr" name="tradingDateStr" value="%{tradingDateStr}" theme="simple" placeholder="  Start day"
    	  class="Wdate"
    	  onFocus="WdatePicker(
    			 {startDate:'%y-%M-%d',dateFmt:'yyyy-MM-dd',alwaysUseStartDate:true,
    			  
    			 	})" >
     </s:textfield>
    </td> 
    <td>
     <s:textfield type="text" id="tradingDateEnd" name="tradingDateEnd" value="%{tradingDateEnd}" theme="simple" placeholder="  End day"
    	  class="Wdate"
    	  onFocus="WdatePicker(
    			 {startDate:'%y-%M-%d',dateFmt:'yyyy-MM-dd',alwaysUseStartDate:true,
    			  
    			 	})" >
     </s:textfield>
    </td>
    
    <td>
      <s:submit onclick="searchStock()" cssClass="comfirm" value="Query" theme="simple"/>
    </td>
      -->
  </tr>
</table>

<!-- high chart -->
<script src="https://code.highcharts.com/stock/highstock.js"></script>
<script src="https://code.highcharts.com/stock/modules/exporting.js"></script>
<div id="container" style="height: 600px; min-width: 310px"></div>
	
  <div id="all">
  <div id="mainFunction">
	<left>
		<table id="grid"></table>
		<div id="gridPager"></div>
	</left>
	</div>
  </div>
</form>
</div>
</div>
</div>
</body>
</html>
 