<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.gtbc.general.bean.*"%><%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>

<link rel="shortcut icon" href="${pageContext.request.contextPath}/images/favicon.ico" type="${pageContext.request.contextPath}/image/x-icon" />

<link href="${pageContext.request.contextPath}/css/displaytag_manage.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-2.2.3.min.js"></script>

<jsp:include page="/jqGridPage.jsp" />

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>GTBC- Historical + Oscillation</title>

<script type="text/javascript">

	$(document).ready(function() {
		$("#grid").jqGrid({
			url:encodeURI('${pageContext.request.contextPath}/general/ItemChartsAction.do?itemId=${itemId}'),
			datatype: "JSON",
			height: "100%+80", 
			width:"960",
			
			colNames:['itemId','ItemName','TdUnixTime', 'TradingDate','Open','High','Low','Volume','Close', 
			          'FastEMA', 'SlowEMA','MACD', 'Signal','HistogramVal', 'ConHistoVal',
			          'MA5','MA10','MA20'],
	        colModel:[
                 {name:'ItemId', index:'ItemId', width:25, align:'center',hidden: true},
                 {name:'ItemName', index:'ItemName', width:25, align:'center',hidden: true},
                 {name:'TdUnixTime', index:'TdUnixTime', width:25, align:'center'}, 
                 {name:'TradingDate', index:'TradingDate', width:25, align:'center'}, 
                 {name:'Open', index:'Open', width:25, align:'right',hidden: false},
                 {name:'High', index:'High', width:25, align:'right',hidden: false},
                 {name:'Low', index:'Low', width:25, align:'right',hidden: false},
                 {name:'Volume', index:'Volume', width:25, align:'right',hidden: false},
                 {name:'Close', index:'Close', width:25, align:'right',hidden: false},
                 {name:'FastEMA', index:'FastEMA', width:25, align:'right'},
                 {name:'SlowEMA', index:'SlowEMA', width:25, align:'right', hidden: false},
                 {name:'MACD', index:'MACD', width:25, align:'right'},
                 {name:'Signal', index:'Signal', width:30, align:'right'},
                 {name:'HistogramVal', index:'HistogramVal',  width:30, align:'right'},
                 {name:'ConHistoVal', index:'ConHistoVal',  width:5, align:'center'},
                 {name:'MA5', index:'MA5',  width:1, align:'center'},
                 {name:'MA10', index:'MA5',  width:1, align:'center'},
                 {name:'MA20', index:'MA5',  width:1, align:'center'},
	        ],
	        sortorder: 'ASC',
	        sortname:'TradingDate',
	        viewrecords: true,
	        gridview: true, 
	        rownumbers: false,
	        rowNum: 1000, 
	        rowList: [1000], 
	        pgbuttons: true,
	        pginput: true,
	        pager: "#gridPager",
	        prmNames:{rows:"pageSize",page:"currentPageNo",sort:"sort",order:"orderBy",total:"totalPages",records:"records",search:"search"}, 
	        //caption: "chart data",
	        loadComplete: function(data) {
                $("#mainFunction").hide();//隱藏jqgrid表單
	        	
	        	var itemName, itemId, i, dummyArr=[];
	        	var itemDataArray=[];
	        	var itemNameId, td=[], c=[], hv=[], ma5=[], ma10=[], ma20=[];
	        	
	        	for (i=0 ; i<data.rows.length; i++)
	        	{	
	        		if(i==0){
	        			itemId = data.rows[0].ItemId;
	        			itemName = data.rows[0].ItemName;
	        			itemNameId = data.rows[0].ItemName + " [" + data.rows[0].ItemId + "]"
	        		}
	        		
	        		// Historical use
	        		itemDataArray[i] = new Array(6);
	        		itemDataArray[i][0] = Number((data.rows[i].TdUnixTime+28800)+'000');	 
	        		itemDataArray[i][1] = data.rows[i].Open;
	        		itemDataArray[i][2] = data.rows[i].High;
	        		itemDataArray[i][3] = data.rows[i].Low;
	        		itemDataArray[i][4] = data.rows[i].Close;
	        		itemDataArray[i][5] = data.rows[i].Volume;
	        		
	        		// Oscillation use
	        		td[i] = data.rows[i].TradingDate;
	        	    c[i] = data.rows[i].Close;
	        	    hv[i] = data.rows[i].HistogramVal;
	        	    ma5[i] = data.rows[i].MA5;
	        	    ma10[i] = data.rows[i].MA10;
	        	    ma20[i] = data.rows[i].MA20;
	        	    
	        	}
	        	
	        	drawHistoricalCharts(itemDataArray, i, itemId, itemName);
	        	drawOscillatorChart(itemNameId, td, c, hv, ma5, ma10, ma20);//forward to highChart!
	        }
	    });
		
	});	


	//highCharts
function drawHistoricalCharts(itemDataArray, counter, itemId, itemName) {

	//theme start by kent >>>>>>>>>>>>>
	/**
	 * Sand-Signika theme for Highcharts JS
	 * @author Torstein Honsi
	 */

	// Load the fonts
	Highcharts.createElement('link', {
	   href: 'https://fonts.googleapis.com/css?family=Signika:400,700',
	   rel: 'stylesheet',
	   type: 'text/css'
	}, null, document.getElementsByTagName('head')[0]);

	// Add the background image to the container
	Highcharts.wrap(Highcharts.Chart.prototype, 'getContainer', function (proceed) {
	   proceed.call(this);
	   this.container.style.background = 'url(http://www.highcharts.com/samples/graphics/sand.png)';
	});


	Highcharts.theme = {
	   colors: ["#f45b5b", "#8085e9", "#8d4654", "#7798BF", "#aaeeee", "#ff0066", "#eeaaee",
	      "#55BF3B", "#DF5353", "#7798BF", "#aaeeee"],
	   chart: {
	      backgroundColor: null,
	      style: {
	         fontFamily: "Signika, serif"
	      }
	   },
	   title: {
	      style: {
	         color: 'black',
	         fontSize: '16px',
	         fontWeight: 'bold'
	      }
	   },
	   subtitle: {
	      style: {
	         color: 'black'
	      }
	   },
	   tooltip: {
	      borderWidth: 0
	   },
	   legend: {
	      itemStyle: {
	         fontWeight: 'bold',
	         fontSize: '13px'
	      }
	   },
	   xAxis: {
	      labels: {
	         style: {
	            color: '#6e6e70'
	         }
	      }
	   },
	   yAxis: {
	      labels: {
	         style: {
	            color: '#6e6e70'
	         }
	      }
	   },
	   plotOptions: {
	      series: {
	         shadow: true
	      },
	      candlestick: {
	         lineColor: '#404048'
	      },
	      map: {
	         shadow: false
	      }
	   },

	   // Highstock specific
	   navigator: {
	      xAxis: {
	         gridLineColor: '#D0D0D8'
	      }
	   },
	   rangeSelector: {
	      buttonTheme: {
	         fill: 'white',
	         stroke: '#C0C0C8',
	         'stroke-width': 1,
	         states: {
	            select: {
	               fill: '#D0D0D8'
	            }
	         }
	      }
	   },
	   scrollbar: {
	      trackBorderColor: '#C0C0C8'
	   },

	   // General
	   background2: '#E0E0E8'

	};
	// Apply the theme
	Highcharts.setOptions(Highcharts.theme);
	//high chart theme END by kent <<<<<<<<<<<<<<
	
   // split the data set into ohlc and volume
   var ohlc = [],
       volume = [],
       dataLength = counter;//data.length,
       
       // set the allowed units for data grouping
       groupingUnits = [[
           'week',                         // unit name
           [1]                             // allowed multiples
       ], [
           'month',
           [1, 2, 3, 4, 6]
       ]],

       i = 0;

   for (i; i < dataLength; i += 1) {
       ohlc.push([
           itemDataArray[i][0], // the date
           itemDataArray[i][1], // open
           itemDataArray[i][2], // high
           itemDataArray[i][3], // low
           itemDataArray[i][4]  // close
       ]);

       volume.push([
           itemDataArray[i][0], // the date
           itemDataArray[i][5]  // the volume
       ]);
   }
   
   // create the chart
   $('#container_his').highcharts('StockChart', {

       rangeSelector: {
           selected: 1
       },
		
       credits: {
           enabled: false //移除highchats右下方logo.
       },
       
       title: {
           text: 'Historical Price'
       },
       subtitle: {
           text:  itemName + ' [' + itemId + ']  © GTBC Inc.'
       },
       yAxis: [{
           labels: {
               align: 'right',
               x: -3
           },
           title: {
               text: '開高低收'//OHLC
           },
           height: '60%',
           lineWidth: 2
       }, {
           labels: {
               align: 'right',
               x: -3
           },
           title: {
               text: '成交量'//VOL
           },
           top: '65%',
           height: '35%',
           offset: 0,
           lineWidth: 2
       }],
       
       series: [{
           type: 'candlestick',
           name: itemName, 
           data: ohlc,
           dataGrouping: {
               units: groupingUnits
           }
       }, {
           type: 'column',
           name: 'Vol',
           data: volume,
           yAxis: 1,
           dataGrouping: {
               units: groupingUnits
           }
       }]
   });
}	
	
//high chart
function drawOscillatorChart(itemNameId, td, c, hv, ma5, ma10, ma20) {

	$('#container_osci').highcharts({
        chart: {
            zoomType: 'xy'
        },
        credits: {
            enabled: false //移除highchats右下方logo.
        },
        title: {
            text: 'Oscillation Analysis Report'
        },
        subtitle: {
            text:  itemNameId + ' © GTBC Inc.' //show StockName and StockId
        },
        xAxis: [{
            categories: td, //TradingDate
            crosshair: true
        }],
        yAxis: [{ // Primary yAxis
            labels: {
                format: '${value}',
                style: {
                    color: "red"
                }
            },
            title: {
                text: 'Close Price',
                style: {
                    color: "red"
                }
            }
        }, { // Secondary yAxis
            title: {
                text: 'Histogram Values',
                style: {
                    color: Highcharts.getOptions().colors[0]
                }
            },
            labels: {
                format: '{value}',
                style: {
                    color: Highcharts.getOptions().colors[0]
                }
            },
            opposite: true
        }],
        tooltip: {
            shared: true
        },
        
        legend: {
            layout: 'vertical',
            align: 'left',
            x: 0,
            verticalAlign: 'top',
            y: 0,
            floating: true,
            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
        },
        
        series: [{
            name: 'Histogram Value',
            type: 'column',
            yAxis: 1,
            data: hv, //Histogram Value
            tooltip: {
                valueSuffix: ''
            }

        }, {
            name: 'Close Price',
            type: 'spline',
            coor: "red",
            data: c,//close
            tooltip: {
                valueSuffix: ''
            }
        }, {
            name: 'MA5',
            type: 'spline',
            yAxis: 0,
            dashStyle: 'shortdot',
            data: ma5,
            color: 'limegreen',
        }, {
            name: 'MA10',
            type: 'spline',
            yAxis: 0,
            dashStyle: 'shortdot',
            data: ma10,
            color: 'orange',
        }, {
            name: 'MA20',
            type: 'spline',
            yAxis: 0,
            dashStyle: 'shortdot',
            data: ma20,
            color: 'lightslategray',
        }]
    });	
    
}
</script>

</head>
<body bgcolor="black">
<!-- 
<div id="grailsLogo"><a href="#"><img src="${pageContext.request.contextPath}/images/gtbc_logo.png" alt="GTBC" border="0" /></a></div>
 -->
<div class="body">
<div class="body">

<div class="dialog">


<form action="" method="post" name="form1" id="form1">
<s:hidden name="itemId" id="itemId" type="text" value="%{itemId}" theme="simple"></s:hidden>

  <!-- high chart -->
  <script src="https://code.highcharts.com/stock/highstock.js"></script>
  <script src="https://code.highcharts.com/stock/modules/exporting.js"></script>
  <div id="container_his" style="min-width: 310px; height: 400px; margin: 0 auto"></div><br>
  <div id="container_osci" style="min-width: 310px; height: 650px; margin: 0 auto"></div><br>
  
  <!-- jqgrid -->
  <div id="all">
  <div id="mainFunction">
	<left>
		<table id="grid"></table>
		<div id="gridPager"></div>
	</left>
	</div>
  </div>

</form>
</div>
</div>
</div>
</body>
</html>
 