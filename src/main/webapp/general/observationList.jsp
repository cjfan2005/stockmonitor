<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.gtbc.stock.bean.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<%@ include file="/IncludePage.jsp"%>
<jsp:include page="/jqGridPage.jsp" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>GTBC-Observation</title>

<script type="text/javascript">
	function clickme(itemId) {		
		var url = '${pageContext.request.contextPath}/general/InitItemChartsAction.do?itemId='+itemId;
		popup(url);
//		$('#form1').attr('action', '${pageContext.request.contextPath}/twStock/InitGenChartFIAction.do?stockId='+stockId);	
//		$('#form1').submit();
	}
	
	function popup(url) {
		params  = 'width='+screen.width;
		params += ', height='+screen.height+200;
		params += ', top=0, left=0'
		params += ', fullscreen=0';
		params += ', scrollbars=yes';
		params += ', location=no';
		
		newwin=window.open(url,'popUpWindow', params);
		if (window.focus) {newwin.focus()}
		return false;
	}
	
	
	$(document).ready(function() {
		$("#bbGrid").jqGrid({
			url:encodeURI('${pageContext.request.contextPath}/general/ObservationListAction.do'),
			//url:'http://query.yahooapis.com/v1/public/yql?format=json&env=store://datatables.org/alltableswithkeys&q=select * from yahoo.finance.quote where symbol in ('+obItems+')',
			datatype: "JSON",
			height: "100%+150", 
			width:"720",
			//colNames:['itemId','Name','今日漲跌','今日最低','今日最高','區間','Vol',''],
			colNames:['itemId','Name',''],
	        colModel:[
                 {name:'ItemId', index:'ItemId', width:10, align:'center',hidden: true, sort:false},
                 {name:'ItemNameId', index:'ItemNameId', width:40, align:'center',hidden: false, sort:false},
                 /* {name:'Change', index:'Change', width:20, align:'right',hidden: false, sort:false},
                 {name:'DaysLow', index:'DaysLow', width:20, align:'right', hidden: false, sort:false},
                 {name:'DaysHigh', index:'DaysHigh', width:20, align:'right', hidden: false, sort:false},
                 {name:'DaysRange', index:'DaysRange', width:25, align:'right', hidden: false, sort:false},
                 {name:'Volume', index:'Volume', width:25, align:'right',hidden: false, sort:false}, */
                 {   //Show Button
               	  name:'action',index:'action',sortable:false, width:20, align:'center',
               	  formatter:function (cellvalue, options, rowObject) {
               	        return '<input class="buttonBlue" type="button" onClick="clickme(\'' + rowObject.ItemId + '\')" type="button" value="Charts"/>'
               	  }
                 },
	        ],
	        
	        viewrecords: true,
	        gridview: true, 
	        rownumbers: true,
	        rowNum: 20, 
	        rowList: [20,40], 
	        pgbuttons: true,
	        pginput: true,
	        pager: "#gridPager",
	        prmNames:{rows:"pageSize",page:"currentPageNo",sort:"sort",order:"orderBy",total:"totalPages",records:"records",search:"search"}, 
	        caption: "Observation List",
	        subGrid : true,
	        loadComplete: function(data) {
	        	$("#ulList1").css("zIndex", 1000);//避免menu圖層蓋住網頁內容,menu顯示在最上層.
	        	$("#ulList2").css("zIndex", 1000);//避免menu圖層蓋住網頁內容,menu顯示在最上層.
	        	$("#ulList3").css("zIndex", 1000);//避免menu圖層蓋住網頁內容,menu顯示在最上層.
	        	$("#ulList4").css("zIndex", 1000);//避免menu圖層蓋住網頁內容,menu顯示在最上層.
	        },
	        
	    	//subGrid start >>>
	    	subGridRowExpanded: function(subgrid_id, row_id) {
				var parentData = jQuery("#bbGrid").jqGrid('getRowData',row_id);
				//alert(parentData.ItemId);
				var subgrid_table_id;
				subgrid_table_id = subgrid_id+"_t";
			 
				$("#"+subgrid_id).html("<table id='"+ subgrid_table_id+"' class='scroll'></table>");
			  	$("#"+subgrid_table_id).jqGrid({
			  		url:"${pageContext.request.contextPath}/general/GetItemIdShortDataAction.do?itemId="+parentData.ItemId,
			  		datatype: "json",
			  		width:"650",
			  		colNames: ['TDate','Open','High','Low','Close','Vol'],
			  		colModel: [
			      		{name:"TradingDate",index:"TradingDate",width:12, align:'center',sortable:false},
			      		{name:"Open",index:"Open",width:15, align:'right', sortable:false},
			      		{name:"High",index:"High",width:15, align:'right', sortable:false},
			      		{name:"Low",index:"Low",width:15, align:'right', sortable:false},
			      		{name:"Price",index:"Price",width:15, align:'right', sortable:false},
			      		{name:"Volume",index:"Volume",width:20, align:'right', sortable:false}
			  		],
			  		height: "100%"+20,
			  		rowNum:5,
			  		prmNames:{rows:"pageSize"},
			  	});
			},
	    	//subgrid end <<<
	    });
	});	
</script>

</head>
<body>
<div id="grailsLogo"><a href="#"><img src="${pageContext.request.contextPath}/images/gtbc_logo.png" alt="GTBC" border="0" /></a></div>
<jsp:include page="/menus_general.jsp"></jsp:include>
<br><br>
<div class="path">
PATH： Personal:
Observation Items</div>

<div class="body">
<div class="body">

<div class="dialog">
<s:actionerror/><s:actionmessage/>

<form action="" method="post" name="form1" id="form1">

<table width="360" >
<%--
  <tr>
    <th>StockId：</th>
    <td>
      <s:textfield name="itemId" id="itemId" type="text" value="%{itemId}" theme="simple"></s:textfield>
    </td>
    <th>Name：</th>
    <td>
      <s:textfield name="name" id="name" type="text" value="%{name}" theme="simple"></s:textfield>
    </td>
    <td>
      <s:submit onclick="searchItem()" cssClass="comfirm" value="Add" theme="simple"/>
    </td>
  </tr>
  --%>
</table>

  <div id="all">
  <div id="mainFunction">
	<left>
		<table id="bbGrid"></table>
		<div id="gridPager"></div>
	</left>
	</div>
  </div>
</form>
</div>
</div>
</div>
</body>
</html>
 